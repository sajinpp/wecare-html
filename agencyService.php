<?php include "master/header.php" ?>

<main>
  <div class="agency_servicepage">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Agency Services
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Agency Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="agency_contents">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Quality and experienced</h3>
                    <p>WCSS is an Approved Labour Hire Agency since 2012. With over 10 years of experience in aged care staffing we have already provided more than 5000 qualified health care professionals serviced over 1 million hours and counting. Our rigorous pre-screening, interview processes, and ongoing training mean that our professional, qualified staff will never let you down. What’s more, our staff are well prepared for a seamless transition and will adapt to your organisation’s culture and processes quickly and effectively.</p>

                    <div class="staffing_box">

                        <div class="whychooseparticle2">
                            <img src="assets/images/whychoose_elipse2.png" alt="">
                        </div>
                        <div class="whychooseparticle1">
                            <img src="assets/images/whychoose_elipse.png" alt="">
                        </div>

                       <div class="staffing_details">
                        <h2>
                            Why choose we care staffing?
                        </h2>
                        <ul>
                            <li>Approved Labour Hire since 2012 - we know the GAME</li>
                            <li>24/7 Phone Support 1300 202 265 so there is ALWAYS someone to take your call</li>
                            <li>30 minute response time to all calls- we understand the URGENCY</li>
                            <li>20+ documentation physically verified for staff- we know COMPLIANCE</li>
                            <li>Metro, Regional, Rural and Remote Locations - to all parts of AUSTRALIA</li>
                        </ul>
                       </div>
                       <div class="staffing_image">
                            <img src="assets/images/staffing.png" alt="">
                       </div>
                    </div>

                

                </div>
            </div>
        </div>
  </div>


  <div class="left_agnecybanner">
    <div class="agency_img">
        <img src="assets/images/agency_leftbanner.png" alt="">
    </div>
    <div class="agency_leftcontents">
        <h3>
            Emergency/Casual Solutions
        </h3>
        <p>
            Whether it be 5am, 12pm or 11pm, our friendly staff are here to take your call for help.
With access to over 2000 staff and a response time of 30 minutes, we act quickly and efficiently to find you the right staff.
We operate a custom rostering software management system that can be accessed on most platforms – Microsoft Windows, Apple OS, and Android. The software system is comprehensive and allows staff to post ongoing availability, reject shifts if necessary, and track progress of shifts and their individual activity.
We have also introduced Client Software, allowing clients such as yourselves to post, monitor and track shifts, as well as access to Staff details, photo ID and Compliance documents.
        </p>
    </div>
</div>

<div class="download_application">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="download_wrapper">
              <div class="download_box">
                  <div class="download_img">
                    <img src="assets/images/wecare_mobileframe.png" alt="">
                  </div>
                  <div class="download_contents">
                    <h6>
                        Client Application
                    </h6>
                    <h2>
                      Download Our Application
                    </h2>
                    <div class="apps">
                      <div class="playstore">
                        <img src="assets/images/icons/google_play.svg" alt="">
                      </div>
                      <div class="playstore">
                        <img src="assets/images/icons/appstore.svg" alt="">
                      </div>
                    </div>
                  </div>
              </div>
              <div class="download_box">
                <div class="download_img">
                  <img src="assets/images/wecare_mobileframe.png" alt="">
                </div>
                <div class="download_contents">
                  <h6>
                    Staff Application
                  </h6>
                  <h2>
                    Download Our Application
                  </h2>
                  <div class="apps">
                    <div class="playstore">
                      <img src="assets/images/icons/google_play.svg" alt="">
                    </div>
                    <div class="playstore">
                      <img src="assets/images/icons/appstore.svg" alt="">
                    </div>
                  </div>
                </div>
            </div>

          </div>
        </div>
      </div>
    </div>
</div>

<div class="short_termSolutions">
    <div class="container">
        <div class="row">
            <div class="col-12 communitycareWrapper">

                <div class="tac_details">
                  <h3>
                    Short Term Solutions
                  </h3>
                  <p>
                    For times when you need qualified and reliable staff for a fixed amount of time. Whether it be to cover Maternity Leave, Long Service Leave or just to fix a short term staffing shortage, WCSS have a range of Short Term solutions for you:
                  </p>

                  <ul>
                    <li>Flexible term options- from a minimum of two weeks.</li>
                    <li>All payroll administrative tasks covered.</li>
                    <li>Flat and agreed rates</li>
                    <li>A panel of at least 3 candidates to choose from</li>
                    <li>All staff qualified, certificated and compliance checked.</li>
                    <li>Fully vaccinated to meet regulatory requirements</li>
                    <li>Replacement guarantee to give you that peace of mind</li>
                    <li>Formal contract in place so there is no grey area</li>
                  </ul>

                </div>
    
                    <div class="tac_image">
                        <img src="assets/images/shortterm_main.png" alt="">
    
                        <div class="sub_image">
                            <img src="assets/images/short_termsub.png" alt="">
                        </div>
    
                    </div>
               </div>
        </div>
    </div>
</div>

<div class="service_promise">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>
                    Service Promise
                </h3>
                <p>
                    At We Care Staffing Solutions, we believe it’s a privilege to assist you.
Drawing on our years of experience in health care, we work hard to deliver quality care that meets your individual needs. Our staff genuinely enjoy working in health care. They are outgoing, friendly and dependable, and show patience and empathy towards both your customers and their families.
                </p>
                <h4>
                    Our promise
                </h4>
                <p>
                    When you engage We Care Staffing Solutions, we promise:
                </p>
                <ul>
                    <li>Access to qualified, enthusiastic and experienced staff who consistently deliver quality care</li>
                    <li>Staff whose skills are carefully matched to your customers’ needs.</li>
                    <li>Reliability of service</li>
                    <li>Rigorous interviewing, screening, reference checking, and ongoing training to ensure the highest quality and compliance of staff</li>
                    <li>A one-stop solution for all your staffing shortages, providing staff for casual through to permanent appointments</li>
                    <li>We take care of all administrative and compliance issues around wages, superannuation, taxation, workers’ compensation, etc., allowing you to focus on core business</li>
                    <li>Client satisfaction – the excellence of our service is recognised by our clients. This is the most valuable yardstick we have, and we’re happy to boast about it!</li>
                    <li>Regional and remote support – we have offices in six locations across Victoria, NSW, and the ACT and we can source staff to support our clients across regional and remote locations</li>
                    <li>24/7 support from our team - we provide the highest quality personnel to cover workdays, nights, weekends, and public holidays. Our lines are also open 24/7 so we’re always available to answer your calls and attend to your requirements.</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="brokerage_serviceSection">
    <div class="brokerage_particle1">
        <img src="assets/images/brokerage_particle1.png" alt="">
    </div>
    <div class="brokerage_particle2">
        <img src="assets/images/brokerage_particle2.png" alt="">
    </div>
    <div class="brokerage_particle2"></div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="brokerage_box">
                    <img src="assets/images/brokerage_image.png" alt="">
                    <button class="brokerage_btn">
                        Brokerage Services
                    </button>
                </div>
                
                <div class="brokerage_boxwrapper">
                    <div class="brokerage_left">
                        <h3>
                            Our People
                        </h3>
                        <p>
                            With over 2000 staff on our books, we are sure to have what you need.
                        </p>
                        
                        <div class="content_boxwrapper">
                            <div class="cotent_box">
                                <h4>
                                    Professionals
                                </h4>

                                <ul>
                                    <li>
                                        Registered Nurses
                                    </li>
                                    <li>
                                        Registered Midwives
                                    </li>
                                    <li>
                                        Registered Psychiatric/Mental Health Nurses
                                    </li>
                                    <li>
                                        Endorsed Enrolled Nurses
                                    </li>
                                    <li>
                                        Assistants in Nursing/Personal Care Assistants
                                    </li>
                                    <li>
                                        Disability Support Workers
                                    </li>
                                    <li>
                                        Food Service Attendants
                                    </li>
                                    <li>
                                        Domestic Assistants
                                    </li>
                                    <li>
                                        CSSD technicians
                                    </li>
                                    <li>
                                        Health Service Assistants/Patients Service Assistants
                                    </li>
                                </ul>


                            </div>

                            <div class="cotent_box">
                                <h4>
                                    Acute Care (Private and Public Hospitals)
                                </h4>

                                <ul>
                                    <li>
                                        Registered Nurses
                                    </li>
                                    <li>
                                        Critical Care Nurses (ICU, HDU, Emergency, CCU, NICU)
                                    </li>
                                    <li>
                                        Registered Psychiatric/Mental Health Nurses
                                    </li>
                                    <li>
                                        Registered Midwives and Special Care Nurses
                                    </li>
                                    <li>
                                        Endorsed Enrolled Nurses/Enrolled Nurses
                                    </li>
                                    <li>
                                        Theatre Nurses
                                    </li>
                                    <li>
                                        Theatre Technicians/CSSD Technicians
                                    </li>
                                    <li>
                                        Patients Service Assistants/Health Service Assistants
                                    </li>
                                    <li>
                                        Cleaners
                                    </li>
                                </ul>


                            </div>

                            <div class="cotent_box">
                                <h4>
                                    Medical Centres/GP Clinics
                                </h4>

                                <ul>
                                    <li>
                                        Registered and Enrolled Nurses
                                    </li>
                                    <li>
                                        Immunisation Nurses
                                    </li>
                                </ul>


                            </div>

                            <div class="cotent_box">
                                <h4>
                                    Community Centres
                                </h4>

                                <ul>
                                    <li>
                                        Registered Nurses
                                    </li>
                                </ul>


                            </div>

                        </div>

                    </div>

                    <div class="brokerage_left">
                        
                        <div class="brokerage_nurseimg">
                            <img src="assets/images/brokerage_nurseimg.png" alt="">
                        </div>
                        
                        <div class="content_boxwrapper">
                            <div class="cotent_box">
                                <h4>
                                    Specialty Services
Aged Care
                                </h4>

                                <ul>
                                    <li>
                                        Registered Nurses
                                    </li>
                                    <li>
                                        Care Managers/Clinical Care Managers
                                    </li>
                                    <li>
                                        Endorsed Enrolled Nurses
                                    </li>
                                    <li>
                                        Assistants in Nursing/Personal Care Assistants
                                    </li>
                                    <li>
                                        Food Service Attendants
                                    </li>
                                    <li>
                                        Domestic Assistants
                                    </li>
                                    <li>
                                        Receptionists
                                    </li>
                                </ul>


                            </div>

                            <div class="cotent_box">
                                <h4>
                                    Rehabilitation Centres
                                </h4>

                                <ul>
                                    <li>
                                        Registered and Enrolled Nurses
                                    </li>
                                    <li>
                                        Personal Care Assistants
                                    </li>
                                    <li>
                                        Patients Service Assistants
                                    </li>
                                    <li>
                                        Cleaners
                                    </li>
                                </ul>


                            </div>

                            

                            <div class="cotent_box">
                                <h4>
                                    Community Nursing
                                </h4>

                                <ul>
                                    <li>
                                        Registered Nurses
                                    </li>
                                </ul>


                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<div class="permenent_solutionSection">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>
                    Need a Permanent Solution
                </h3>
                <p>
                    Just don’t have the time or patience to advertise, interview and recruit your own staff? WCSS provide permanent placement solutions.
Our specialised team of recruitment consultants have a wealth of experience in recruitment and in healthcare to provide you with the confidence you get the right person. We provide the following services, including:
                </p>

              <div class="peace_candidatewrapper">
                <div class="candidate_box">
                    <ul>
                        <li>
                            Candidate search
                        </li>
                        <li>
                            Job posting and screening.
                        </li>
                        <li>
                            Resume submission
                        </li>
                        <li>
                            Interview scheduling
                        </li>
                        <li>
                            Reference checking
                        </li>
                        <li>
                            Compliance checking
                        </li>
                    </ul>
                </div>

                <div class="peace_mind">
                    <p>
                        For your peace of mind, all candidates come with:
                    </p>
                    <ul>
                        <li>ID and screening checks</li>
                        <li>Full compliance documentation</li>
                        <li>Necessary registrations</li>
                        <li>Replacement Guarantee</li>
                    </ul>
                </div>
              </div>

              <div class="international_recruitment">
                <h3>
                    International Recruitment Services
                </h3>
                <p>
                    With over 15 years of experience in Overseas Nursing recruitment we are a trusted company who pride ourselves on the quality of our service and of the staff provided. Get access to Nurses from:
                </p>

                <div class="location_image">
                    <img src="assets/images/location_image.png" alt="">
                </div>

                <div class="peace_candidatewrapper last_candidate" >
                    <div class="candidate_box">
                        <h4>
                            Full Recruitment Services:
                        </h4>
                        <ul>
                            <li>
                                Minimum 3 candidates presented.
                            </li>
                            <li>
                                Experienced and Qualified
                            </li>
                            <li>
                                AHPRA Registered and ANMAC Assessed
                            </li>
                            <li>
                                Visa Assistance/Migration Services
                            </li>
                            <li>
                                Assistance with travel and accommodation
                            </li>
                        </ul>
                    </div>
    
                    <div class="candidate_box">
                        <h4>
                            For all Nursing needs
                        </h4>
                        <ul>
                            <li>
                                Registered Nurses
                            </li>
                            <li>
                                Medical and Surgical Nurses
                            </li>
                            <li>
                                Theatre/Recovery/Radiology Nurses
                            </li>
                            <li>
                                Critical Care Nurses (ICU, HDU, Emergency, CCU, NICU)
                            </li>
                        </ul>
                    </div>
                  </div>


              </div>

           


            </div>
        </div>
    </div>
</div>

<div class="service_booking_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="booking_box">
                    <div class="booking_details">
                        <h3>
                            Book a service now
                        </h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt .
                        </p>
                        <div class="booking_banner">
                            <img src="assets/images/booking_banner.png" alt="">
                        </div>
                    </div>
                    <div class="booking_form">
                        <form action="">
                            <div class="input_parent">
                                <input type="text" placeholder="Full Name">
                            </div>
                            <div class="input_parent">
                                <input type="text" placeholder="Email ID">
                            </div>
                            <div class="input_parent">
                                <input type="text" placeholder="Phone Number">
                            </div>
                            <div class="input_parent">
                                <textarea name="" id=""></textarea>
                            </div>
                            <div class="send_btn">
                                <button class="primary_btn">
                                    Send Message
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

  <div class="map_wrapper">
    <img src="assets/images/map.png" alt="">
  </div>

</main>

<?php include 'master/footer.php' ?>

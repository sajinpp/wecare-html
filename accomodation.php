<?php include "master/header.php" ?>

<main>
  <div class="contact_banner accomodation_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Accomodation
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Accomodation</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="accomodation_page">
    <div class="particle">
        <img src="assets/images/vhc_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
           <div class="col-12 accomodation_wrapper">

            <div class="tac_details">
              
                <h3>
                    Supported Independent Living (SIL) Services
                </h3>
                <p>
                    SIL services fall under the core NDIS category of Assistance with Daily Life and are funded by the NDIS to assist participants to live as independently as possible and to enhance their own daily living skills. They are available for participants living:
                </p>

                <ul>
                    <li>
                        On their own
                    </li>
                    <li>
                        With other NDIS participants in shared or group accommodation
                    </li>
                    <li>
                        In Special Disability Accommodation (SDA)
                    </li>
                    <li>
                        In privately rented homes
                    </li>
                    <li>
                        In homes that they own
                    </li>
                    <li>
                        In housing provided by the Department of Housing or similar services/agencies.
                    </li>
                </ul>

                <p>
                    SIL services include such things such as personal care and meal preparation, but they differ from other assisted living support services in that they cater for participants with higher support needs who require some level of in-home help at all times – seven days a week, for all or most of the day. Overnight support is also included.
                </p>

                <p>
                    SIL providers can also assist with outside home community access, and with any issues or concerns about other support services participants are receiving.
                </p>

                <h3 class="why_choosehead">
                    Why choose We Care Staffing Solutions for your SIL services?
                </h3>
                <p>
                    As an approved provider of SIL services under the NDIS we have accommodation houses across New South Wales and Victoria, and we supply qualified, friendly, and efficient support staff to assist participants with daily living tasks, and to help build and develop their own skills in dealing with these tasks.
                </p>

                <p>
                    We are dedicated to helping participants live their most independent and fulfilling lives on their own terms. Our extensive support worker training and skills matching expertise ensures that all participants will receive the highest quality of consistent care from a support worker who understands their challenges and needs and who facilitates their current and future goals.
                </p>

                <h5>
                    Please click on this link to see our current vacancies! 
                </h5>
                <p>
                    By helping you find the right accommodation we can also help you to continue your journey to living your best life on your terms. As well as more permanent options, there are also opportunities for short-term (STA) and medium-term accommodation (MTA) – this can be helpful in finding out what sort of residence is best suited to your needs. MTA can also be an option if there is a waiting period for moving into permanent or ongoing accommodation, or when waiting for modifications to a more permanent residence are completed.
                </p>
                <p>
                    STA is funded under a participant’s plan for up to 28 days. MTA is funded for up to three months. Our friendly and knowledgeable staff can assist with some advice and recommendations if you are considering these options.
                </p>

                <p>
                    As part of our SIL services, these are some of the tasks and activities our friendly, qualified support workers can assist with:
                </p>
            
                <ul>
                    <li>Cooking and meal preparation</li>
                    <li>Personal care and hygiene</li>
                    <li>Cleaning and laundry</li>
                    <li>Socializing and making friends</li>
                    <li>Attending community events</li>
                    <li>Working and volunteering.</li>
                </ul>

                <p>
                    Assistance with these and other similar things, as often as you require, can make living on your own or in shared accommodation the ideal situation for a busy and involved lifestyle! Our support workers are also trained to help you learn to master these things on your own, for increased ongoing independence and confidence.
                </p>

                <h6>
                    Please see our ‘Contact Us’ page and get in touch if you have any questions about eligibility or funding for SIL services. We will be happy to address these and any other questions for participants and/or their carers or family members.
                </h6>


                <div class="patient_boxwrapper">
                    <div class="patient_box">
                        <div class="patient_image">
                            <img src="assets/images/patient_image.png" alt="">
                        </div>
                        <div class="patient_details">
                            <h4>
                                Penrith NSW 2750
                            </h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                            </p>

                            <div class="feature_list">
                                <ul>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Bedrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bathrooms.svg" alt="">
                                            <span>Bathrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            2
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/vaccancy.svg" alt="">
                                            <span>Vacancies  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Wheelchair  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            Accessible
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/parking.svg" alt="">
                                            <span>Car Parking  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                </ul>

                               

                            </div>

                            <div class="enquire_btn">
                                <button>
                                    Enquire Now
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="patient_box">
                        <div class="patient_image">
                            <img src="assets/images/patient_image.png" alt="">
                        </div>
                        <div class="patient_details">
                            <h4>
                                Penrith NSW 2750
                            </h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                            </p>

                            <div class="feature_list">
                                <ul>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Bedrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bathrooms.svg" alt="">
                                            <span>Bathrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            2
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/vaccancy.svg" alt="">
                                            <span>Vacancies  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Wheelchair  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            Accessible
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/parking.svg" alt="">
                                            <span>Car Parking  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                </ul>

                               

                            </div>

                            <div class="enquire_btn">
                                <button>
                                    Enquire Now
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="patient_box">
                        <div class="patient_image">
                            <img src="assets/images/patient_image.png" alt="">
                        </div>
                        <div class="patient_details">
                            <h4>
                                Penrith NSW 2750
                            </h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                            </p>

                            <div class="feature_list">
                                <ul>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Bedrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bathrooms.svg" alt="">
                                            <span>Bathrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            2
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/vaccancy.svg" alt="">
                                            <span>Vacancies  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Wheelchair  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            Accessible
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/parking.svg" alt="">
                                            <span>Car Parking  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                </ul>

                               

                            </div>

                            <div class="enquire_btn">
                                <button>
                                    Enquire Now
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="patient_box">
                        <div class="patient_image">
                            <img src="assets/images/patient_image.png" alt="">
                        </div>
                        <div class="patient_details">
                            <h4>
                                Penrith NSW 2750
                            </h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                            </p>

                            <div class="feature_list">
                                <ul>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Bedrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bathrooms.svg" alt="">
                                            <span>Bathrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            2
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/vaccancy.svg" alt="">
                                            <span>Vacancies  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Wheelchair  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            Accessible
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/parking.svg" alt="">
                                            <span>Car Parking  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                </ul>

                               

                            </div>

                            <div class="enquire_btn">
                                <button>
                                    Enquire Now
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="patient_box">
                        <div class="patient_image">
                            <img src="assets/images/patient_image.png" alt="">
                        </div>
                        <div class="patient_details">
                            <h4>
                                Penrith NSW 2750
                            </h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                            </p>

                            <div class="feature_list">
                                <ul>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Bedrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bathrooms.svg" alt="">
                                            <span>Bathrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            2
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/vaccancy.svg" alt="">
                                            <span>Vacancies  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Wheelchair  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            Accessible
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/parking.svg" alt="">
                                            <span>Car Parking  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                </ul>

                               

                            </div>

                            <div class="enquire_btn">
                                <button>
                                    Enquire Now
                                </button>
                            </div>

                        </div>
                    </div>
                    <div class="patient_box">
                        <div class="patient_image">
                            <img src="assets/images/patient_image.png" alt="">
                        </div>
                        <div class="patient_details">
                            <h4>
                                Penrith NSW 2750
                            </h4>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text.
                            </p>

                            <div class="feature_list">
                                <ul>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Bedrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bathrooms.svg" alt="">
                                            <span>Bathrooms  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            2
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/vaccancy.svg" alt="">
                                            <span>Vacancies  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/bed.svg" alt="">
                                            <span>Wheelchair  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            Accessible
                                        </h6>
                                    </li>
                                    <li>
                                        <h5>
                                            <img src="assets/images/icons/parking.svg" alt="">
                                            <span>Car Parking  </span>
                                        </h5>
                                        <div class="semicolumn">
                                            :
                                        </div>
                                        <h6>
                                            3
                                        </h6>
                                    </li>
                                </ul>

                               

                            </div>

                            <div class="enquire_btn">
                                <button>
                                    Enquire Now
                                </button>
                            </div>

                        </div>
                    </div>
                </div>

            

            </div>
           </div>
        </div>
    </div>



  </div>






</main>

<?php include 'master/footer.php' ?>

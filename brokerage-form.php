<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Brokerage Services
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Brokerage Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="brokerage_formwrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="brokerage_clientform">
                    <h3>
                        Clients Details
                    </h3>
                    <form action="">
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Full Name<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Date Of Birth<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Phone Number<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Email Address<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="single_inputgroup">
                            <div class="input_group">
                                <label for="">Street Address<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">City<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">State<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Post Code<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="brokerage_clientform">
                    <h3>
                        Client Representative Details <span>( If Applicable )</span>
                    </h3>
                    <form action="">
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Full Name<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Phone Number<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Email Address<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="single_inputgroup">
                            <div class="input_group">
                                <label for="">Street Address<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">City<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">State<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Post Code<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="brokerage_clientform">
                    <h3>
                        NDIS Details
                    </h3>
                    <form action="">

                        <div class="radio_multiselect">
                            <label for="">Plan  <sup>*</sup></label>

                            <div class="radio_wrapper">
                                <input type="radio" name="chooseone" id="">
                                <h6>Plan Managed</h6>
                            </div>
                            <div class="radio_wrapper">
                                <input type="radio" name="chooseone" id="">
                                <h6>Self Managed</h6>
                            </div>
                            <div class="radio_wrapper">
                                <input type="radio" name="chooseone" id="">
                                <h6>Agency Managed</h6>
                            </div>

                        </div>

                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Plan Manager Name <span>( If Applicable )</span></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Plan Manager Agency <span>( If Applicable )</span></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">NDIS Number<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Available/ Remaining Funding  for Capacity Building Supports</label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Plan Start Date<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Plan Review Date<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Client Goals <span>( As Started in the NDIS Plan )</span></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="brokerage_clientform">
                    <h3>
                        Referrer Details <span>( Person Making the Referral )</span>
                    </h3>
                    <form action="">
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Full Name<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Agency</label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Role</label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                            <div class="input_group">
                                <label for="">Phone Number<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <label for="">Email Address<sup>*</sup></label>
                                <div class="input_parent">
                                    <input type="text">
                                </div>
                            </div>
                        </div>

                        <div class="terms_tick">
                            <input type="checkbox">
                            <p>
                                I have obtained consent from the participant to make this referral and provide Compass Physiotherapy with the participant's personal
and medical details. <sup>*</sup>
                            </p>
                        </div>

                    </form>
                </div>
                <div class="brokerage_clientform">
                    <h3>
                        Reason For Referral
                    </h3>
                    <form action="">

                        <div class="radio_multiselect">
                            <label for="">Referred For<sup>*</sup></label>

                            <div class="radio_wrapper">
                                <input type="radio" name="referchoose" id="">
                                <h6>Physiotherapy</h6>
                            </div>
                            <div class="radio_wrapper">
                                <input type="radio" name="referchoose" id="">
                                <h6>Chiro</h6>
                            </div>
                            <div class="radio_wrapper">
                                <input type="radio" name="referchoose" id="">
                                <h6>Psychologist</h6>
                            </div>
                            <div class="radio_wrapper">
                                <input type="radio" name="referchoose" id="">
                                <h6>Other</h6>
                            </div>

                        </div>

                        <div class="single_inputgroup">
                            <div class="input_group">
                                <label for="">Reason For Referral/Relevant Medical Information<sup>*</sup></label>
                                <div class="input_parent">
                                    <textarea placeholder="Type here" name="" id=""></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="multi_inputgroup">
                            <div class="input_group">
                                <div class="file_upload">
                                    <label for="">File Upload <span> (Please attach a copy of the current NDIS plan if possible)</span></label>
        
                                    <div class="upload_box">
                                        <h6>
                                            Browse
                                        </h6>
                                    </div>
        
                                </div>
                            </div>
                        </div>

                      
                        

                    </form>
                </div>

                <div class="submt_wrapper">
                    <button class="primary_btn">
                        Submit
                    </button>
                </div>

            </div>
        </div>
    </div>
  </div>







</main>

<?php include 'master/footer.php' ?>

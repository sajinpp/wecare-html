<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    VHC
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">VHC</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="vhc_detailpage">
    <div class="particle">
        <img src="assets/images/vhc_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
           <div class="col-12 vhv_contents">
                <p>
                    We are an approved provider of the Department of Veteran Affairs (DVA) to provide their Veteran Home Care Program (VHC) to eligible veterans in various regions.
                </p>
                <h5>
                    Veterans' Home Care (VHC) assessment agencies and service providers | Department of Veterans' Affairs (dva.gov.au)
                </h5>
                <p>
                    The Veterans' Home Care program helps eligible veterans and their dependents who need low-level care at their own homes. They must have a Veteran Gold Card or Veteran White Card.
                </p>

                <div class="vhc_image">
                    <img src="assets/images/vhc_image.png" alt="">
                </div>

                <p>
                    The VHC program provides home care services to maintain the health, wellbeing and independence of eligible veterans and their families.
                </p>

                <p>
                    The VHC service includes:
                </p>

                <ul>
                    <li>
                        Domestic help
                    </li>
                    <li>
                        Personal care
                    </li>
                    <li>
                        Respite care
                    </li>
                    <li>
                        Home and garden maintenance (safety-related)
                    </li>
                    <li>
                        Attending community events
                    </li>
                    <li>
                        Lawn mowing (does not need to be safety-related)
                    </li>
                </ul>

           </div>
        </div>
    </div>



  </div>






</main>

<?php include 'master/footer.php' ?>

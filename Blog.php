<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Our Blogs
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Our Blogs</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="blog_contents">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Check out our blogs for more information
                </h2>
                <p>
                    Blandit dignissim mattis nibh est hac vitae enim integer at. Tempus egestas ultrices eget volutpat volutpat. Pulvinar ut lectus libero hendrerit ac est egestas sed.
                </p>

                <ul class="blog_listing">
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog1.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog2.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog3.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog2.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog1.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog3.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog1.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog2.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                    <li><a href="BlogDetails.php">
                        <div class="blog_image">
                            <img src="assets/images/blog3.png" alt="">
                        </div>
                        <div class="blog_contents">
                            <h3>
                                The Art of Communication in Nursing: Why It Matters More Than Ever
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, Lorem ipsum dolor sit amet,. <span>Read More</span>
                            </p>
                        </div>
                    </a></li>
                </ul>

                <div class="common_Pagination">
                    <div class="prev">
                        <img src="assets/images/icons/prev_right.svg" alt="">
                        PREVIOUS
                    </div>
                    <ul>
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">5</a></li>
                        <li><a href="">6</a></li>
                        <li><a href="">7</a></li>
                    </ul>
                    <div class="next">
                        NEXT
                        <img src="assets/images/icons/prev_left.svg" alt="">
                       
                    </div>
                </div>


            </div>
        </div>
    </div>
  </div>






</main>

<?php include 'master/footer.php' ?>

<?php include "master/header.php" ?>

<main>
  <section class="banner_section">
    <div class="banner_slideWrapper">
      <div class="swiper banner_slider">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <div class="banner_background">
                <img src="assets/images/banner.png" alt="" />

                <div class="refer_label">
                  <a href="brokerage-form.php">referral form</a>
                </div>

                <div class="container banner_container">
                    <div class="row">
                        <div class="col-12">
                            <div class="banner_contents">
                                <h1>
                                    NDIS and Disability Services
                                </h1>
                                <p>
                                    We provide Accomodation setting, In-home and
Community Access supports under NDIS, SIL and other funded
programs.
                                </p>
                                <div class="button_wrapper">
                                    <button class="primary_btn">
                                        Find out more
                                    </button>
                                    <button class="primary_btn">
                                        Enquire now
                                    </button>
                                </div>

                                <ul class="agency_list">
                                  <li>
                                    NDIS & disability Service
                                  </li>
                                  <li>
                                    SIL / SDA
                                  </li>
                                  <li>agency services</li>
                                  <li>
                                    Partnership Opportunities
                                  </li>
                                  <li>work with Us</li>
                                </ul>

                            </div>
                        </div>
                    </div>
                </div>

              </div>
          </div>
          <div class="swiper-slide">
            <div class="banner_background">
              <img src="assets/images/banner.png" alt="" />

              <div class="refer_label">
                referral form
              </div>

              <div class="container banner_container">
                  <div class="row">
                      <div class="col-12">
                          <div class="banner_contents">
                              <h1>
                                  NDIS and Disability Services
                              </h1>
                              <p>
                                  We provide Accomodation setting, In-home and
Community Access supports under NDIS, SIL and other funded
programs.
                              </p>
                              <div class="button_wrapper">
                                  <button class="primary_btn">
                                      Find out more
                                  </button>
                                  <button class="primary_btn">
                                      Enquire now
                                  </button>
                              </div>
                              <ul class="agency_list">
                                <li>
                                  NDIS & disability Service
                                </li>
                                <li>
                                  SIL / SDA
                                </li>
                                <li>agency services</li>
                                <li>
                                  Partnership Opportunities
                                </li>
                                <li>work with Us</li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>

            </div>
          </div>
          <div class="swiper-slide">
            <div class="banner_background">
              <img src="assets/images/banner.png" alt="" />

              <div class="refer_label">
                referral form
              </div>

              <div class="container banner_container">
                  <div class="row">
                      <div class="col-12">
                          <div class="banner_contents">
                              <h1>
                                  NDIS and Disability Services
                              </h1>
                              <p>
                                  We provide Accomodation setting, In-home and
Community Access supports under NDIS, SIL and other funded
programs.
                              </p>
                              <div class="button_wrapper">
                                  <button class="primary_btn">
                                      Find out more
                                  </button>
                                  <button class="primary_btn">
                                      Enquire now
                                  </button>
                              </div>
                              <ul class="agency_list">
                                <li>
                                  NDIS & disability Service
                                </li>
                                <li>
                                  SIL / SDA
                                </li>
                                <li>agency services</li>
                                <li>
                                  Partnership Opportunities
                                </li>
                                <li>work with Us</li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>

            </div>
          </div>
          <div class="swiper-slide">
            <div class="banner_background">
              <img src="assets/images/banner.png" alt="" />

              <div class="refer_label">
                referral form
              </div>

              <div class="container banner_container">
                  <div class="row">
                      <div class="col-12">
                          <div class="banner_contents">
                              <h1>
                                  NDIS and Disability Services
                              </h1>
                              <p>
                                  We provide Accomodation setting, In-home and
Community Access supports under NDIS, SIL and other funded
programs.
                              </p>
                              <div class="button_wrapper">
                                  <button class="primary_btn">
                                      Find out more
                                  </button>
                                  <button class="primary_btn">
                                      Enquire now
                                  </button>
                              </div>
                              <ul class="agency_list">
                                <li>
                                  NDIS & disability Service
                                </li>
                                <li>
                                  SIL / SDA
                                </li>
                                <li>agency services</li>
                                <li>
                                  Partnership Opportunities
                                </li>
                                <li>work with Us</li>
                              </ul>
                          </div>
                      </div>
                  </div>
              </div>

            </div>
          </div>
        </div>
        <div class="swiper-button-next" id="banner_next"></div>
        <div class="swiper-button-prev" id="banner_prev"></div>
      </div>
    </div>
  </section>
  <section class="about_section">
    <div class="container">
      <div class="row">
        <div class="col-12">
            <div class="about_wrapper">
              <div class="about_imagewrapper">
                <div class="sub_image">
                  <img src="assets/images/about_main.png" alt="">

                  <div class="patient_box">
                    <div class="patient_img">
                      <img src="assets/images/icons/patient_count.svg" alt="">
                    </div>
                    <div class="patient_contents">
                      <h5>
                        1,345 +
                      </h5>
                      <h6>
                        Happy Patients
                      </h6>
                    </div>
                  </div>

                </div>
                <div class="sub_image">
                  <img src="assets/images/about_secondSub.png" alt="">
                </div>
              </div>
              <div class="about_contents">
                <h6 class="sub_heading">
                  about Us
                </h6>
                <h2 class="heading">
                  Most Important Thing for us is Love & Care
                </h2>
                <p class="paragraph">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                </p>
                <button class="primary_btn">
                  about Us 
                </button>
              </div>
            </div>
        </div>
      </div>
    </div>
  </section>

  <section class="team_section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="team_boxwrapper">
              <h2>
                Join Our Team
              </h2>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              </p>

              <div class="button_wrapper">
                <button class="primary_btn">
                  Join now 
                </button>
                <button class="primary_btn">
                  See Current Vacancies
                </button>
              </div>

          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="download_application">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="download_wrapper">
              <div class="download_box">
                  <div class="download_img">
                    <img src="assets/images/wecare_mobileframe.png" alt="">
                  </div>
                  <div class="download_contents">
                    <h6>
                        Client Application
                    </h6>
                    <h2>
                      Download Our Application
                    </h2>
                    <div class="apps">
                      <div class="playstore">
                        <img src="assets/images/icons/google_play.svg" alt="">
                      </div>
                      <div class="playstore">
                        <img src="assets/images/icons/appstore.svg" alt="">
                      </div>
                    </div>
                  </div>
              </div>
              <div class="download_box">
                <div class="download_img">
                  <img src="assets/images/wecare_mobileframe.png" alt="">
                </div>
                <div class="download_contents">
                  <h6>
                    Staff Application
                  </h6>
                  <h2>
                    Download Our Application
                  </h2>
                  <div class="apps">
                    <div class="playstore">
                      <img src="assets/images/icons/google_play.svg" alt="">
                    </div>
                    <div class="playstore">
                      <img src="assets/images/icons/appstore.svg" alt="">
                    </div>
                  </div>
                </div>
            </div>

          </div>
          <div class="tutorial_video">
            <iframe width="100%" height="400" src="https://www.youtube.com/embed/YWA-xbsJrVg?si=FN2F1ADlmEBMa0Vx" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="testimonials_section">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="testimonial_head">
            <div class="titles">
              <h6>
                Testimonials 
              </h6>
              <h2>
                What People say About Us
              </h2>
            </div>
            <div class="description">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam, quis nostrud exercitation ullamco.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="swiper testimonial_swiper">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                <div class="testimonial_box">
                  <div class="user_wrapper">
                      <div class="user_details">
                          <div class="user_image">
                              <img src="assets/images/Avatar.png" alt="">
                          </div>
                          <div class="user_name">
                              <h5>
                                  Sean Farmer
                              </h5>
                              <h6>
                                  Job title
                              </h6>
                          </div>
                      </div>
                      <div class="quote">
                          <img src="assets/images/quote.svg" alt="">
                      </div>
                  </div>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                  </p>

                  <div class="youtube_video">
                      <img src="assets/images/youtube.svg" alt="">
                      <h6>
                          Video testimonial
                      </h6>
                  </div>

                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial_box">
                  <div class="user_wrapper">
                      <div class="user_details">
                          <div class="user_image">
                              <img src="assets/images/Avatar.png" alt="">
                          </div>
                          <div class="user_name">
                              <h5>
                                  Sean Farmer
                              </h5>
                              <h6>
                                  Job title
                              </h6>
                          </div>
                      </div>
                      <div class="quote">
                          <img src="assets/images/quote.svg" alt="">
                      </div>
                  </div>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                  </p>

                  <div class="youtube_video">
                      <img src="assets/images/youtube.svg" alt="">
                      <h6>
                          Video testimonial
                      </h6>
                  </div>

                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial_box">
                  <div class="user_wrapper">
                      <div class="user_details">
                          <div class="user_image">
                              <img src="assets/images/Avatar.png" alt="">
                          </div>
                          <div class="user_name">
                              <h5>
                                  Sean Farmer
                              </h5>
                              <h6>
                                  Job title
                              </h6>
                          </div>
                      </div>
                      <div class="quote">
                          <img src="assets/images/quote.svg" alt="">
                      </div>
                  </div>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                  </p>

                  <div class="youtube_video">
                      <img src="assets/images/youtube.svg" alt="">
                      <h6>
                          Video testimonial
                      </h6>
                  </div>

                </div>
              </div>
              <div class="swiper-slide">
                <div class="testimonial_box">
                  <div class="user_wrapper">
                      <div class="user_details">
                          <div class="user_image">
                              <img src="assets/images/Avatar.png" alt="">
                          </div>
                          <div class="user_name">
                              <h5>
                                  Sean Farmer
                              </h5>
                              <h6>
                                  Job title
                              </h6>
                          </div>
                      </div>
                      <div class="quote">
                          <img src="assets/images/quote.svg" alt="">
                      </div>
                  </div>
                  <p>
                      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                  </p>

                  <div class="youtube_video">
                      <img src="assets/images/youtube.svg" alt="">
                      <h6>
                          Video testimonial
                      </h6>
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </section>

  <section class="values_section">
    <div class="knowmore_particle">
      <img src="assets/images/knowmore_particle.png" alt="">
    </div>
    <div class="knowmore_particle2">
      <img src="assets/images/knowmore_particle2.png" alt="">
    </div>
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h6>
            Our Values 
          </h6>
          <h2>
            Know us by the numbers section
          </h2>

          <ul class="count_listing">
            <li>
              <img src="assets/images/icons/operation.svg" alt="">
              <h2>
                  12
              </h2>
              <h6>
                Years in Operation
              </h6>
            </li>
            <li>
              <img src="assets/images/icons/clients.svg" alt="">
              <h2>
                  2000
              </h2>
              <h6>
                Clients Serviced
              </h6>
            </li>
            <li>
              <img src="assets/images/icons/employees.svg" alt="">
              <h2>
                  1500
              </h2>
              <h6>
                Active Employees
              </h6>
            </li>
            <li>
              <img src="assets/images/icons/hours.svg" alt="">
              <h2>
                1,000,000
              </h2>
              <h6>
                Total Services Hours
              </h6>
            </li>
          </ul>

        </div>
      </div>
    </div>
  </section>

  <div class="map_wrapper">
    <img src="assets/images/map.png" alt="">
  </div>

</main>

<?php include 'master/footer.php' ?>

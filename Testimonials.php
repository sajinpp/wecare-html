<?php include "master/header.php" ?>

<main>
  <div class="contact_banner testimonial_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Testimonials 
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Testimonials</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>


  <div class="testimonials_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="testimonial_listing">
                    <h2>
                        what people say about Us
                    </h2>

                    <ul class="testimonial_boxWrapper">
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>
                        <li class="testimonial_box">
                            <div class="user_wrapper">
                                <div class="user_details">
                                    <div class="user_image">
                                        <img src="assets/images/Avatar.png" alt="">
                                    </div>
                                    <div class="user_name">
                                        <h5>
                                            Sean Farmer
                                        </h5>
                                        <h6>
                                            Job title
                                        </h6>
                                    </div>
                                </div>
                                <div class="quote">
                                    <img src="assets/images/quote.svg" alt="">
                                </div>
                            </div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam.Lorem ipsum dolor.
                            </p>

                            <div class="youtube_video">
                                <img src="assets/images/youtube.svg" alt="">
                                <h6>
                                    Video testimonial
                                </h6>
                            </div>

                        </li>

                    </ul>


                    
                <div class="common_Pagination">
                    <div class="prev">
                        <img src="assets/images/icons/prev_right.svg" alt="">
                        PREVIOUS
                    </div>
                    <ul>
                        <li class="active"><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">5</a></li>
                        <li><a href="">6</a></li>
                        <li><a href="">7</a></li>
                    </ul>
                    <div class="next">
                        NEXT
                        <img src="assets/images/icons/prev_left.svg" alt="">
                       
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
  </div>





</main>

<?php include 'master/footer.php' ?>

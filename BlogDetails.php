<?php include "master/header.php" ?>

<main>
    <div class="blogDetail_Page">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h6>
                        30 JUNE 2023
                    </h6>
                    <h2>
                        The Art of Communication in Nursing: Why It Matters More Than Ever
                    </h2>
                    <div class="blogdetail_image">
                        <img src="assets/images/blogDetail_image.png" alt="">
                    </div>

                    <p>
                        Gravida nibh arcu sit donec. Venenatis odio sed elementum quis tristique scelerisque molestie quam risus. Egestas aliquet semper libero quam vitae ut. Id sit enim est at duis diam. Id lectus natoque risus ultrices aliquam mauris consectetur. Hac eu mollis elementum at aenean amet. Magna vehicula in ipsum in. Laoreet tempus aenean id neque ornare sit ipsum amet. Consectetur egestas platea eu nibh proin fusce nullam. Amet libero lorem vel odio quis feugiat et euismod mauris. Scelerisque est a tincidunt sed arcu ullamcorper. Ut nunc at arcu nulla elit tempor pellentesque tellus.
                    </p>
                    <p>
                        Facilisi leo ut neque nunc mi ultricies. Faucibus in curabitur sapien donec sit scelerisque. Id nulla amet vitae posuere odio convallis risus. Viverra cras nulla amet pellentesque quis. Id id dignissim hac neque dictumst integer blandit. Leo ac adipiscing pretium venenatis sem arcu.
                    </p>

                    <h3>
                        Hac eu mollis elementum
                    </h3>
                    <p>
                        Gravida nibh arcu sit donec. Venenatis odio sed elementum quis tristique scelerisque molestie quam risus. Egestas aliquet semper libero quam vitae ut. Id sit enim est at duis diam. Id lectus natoque risus ultrices aliquam mauris consectetur. Hac eu mollis elementum at aenean amet. Magna vehicula in ipsum in. Laoreet tempus aenean id neque ornare sit ipsum amet. Consectetur egestas platea eu nibh proin fusce nullam. Amet libero lorem vel odio quis feugiat et euismod mauris. Scelerisque est a tincidunt sed arcu ullamcorper. Ut nunc at arcu nulla elit tempor pellentesque tellus.
                    </p>
                    <p>
                        Facilisi leo ut neque nunc mi ultricies. Faucibus in curabitur sapien donec sit scelerisque. Id nulla amet vitae posuere odio convallis risus. Viverra cras nulla amet pellentesque quis. Id id dignissim hac neque dictumst integer blandit. Leo ac adipiscing pretium venenatis sem 
                    </p>

                    <div class="blogdetail_image">
                        <img src="assets/images/blogDetail2.png" alt="">
                    </div>

                    <h3>
                        Orci amet tincidunt metus
                    </h3>
                    <p>
                        Dui mus ullamcorper id sem. Sed sed augue aliquam volutpat et magna convallis. Pretium velit amet ultricies fusce condimentum. Aliquet euismod vitae cursus donec. Sit convallis venenatis vivamus massa sagittis enim leo euismod viverra. Orci amet tincidunt metus elit elit sed vel quis nisi. Ante egestas consequat volutpat faucibus purus lacus accumsan. Enim massa nascetur et aenean non turpis ornare.
Viverra rhoncus varius sem id et varius arcu et. Mauris fringilla gravida elit scelerisque tellus. Pellentesque lorem molestie consequat nec massa nibh. Mauris nibh lacinia at pulvinar. Neque aliquam amet netus interdum. Sed nam vel in ut nunc. Egestas molestie feugiat elementum non sit eget euismod. Elit donec pellentesque molestie vel nunc in elit. Interdum nulla at nisl interdum. Varius id purus duis magna turpis felis ut ut tristique.
Nulla sit volutpat commodo sed varius dictum etiam. A ipsum etiam suspendisse elit. Ultricies mauris quis convallis in.
                    </p>

                </div>
            </div>
        </div>
    </div>
</main>

<?php include 'master/footer.php' ?>

<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Home and Community Care Services
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Home and Community Care Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="CommunityCare">
    <div class="particle">
        <img src="assets/images/vhc_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
           <div class="col-12 communitycareWrapper">

            <div class="tac_details">
              <p>
                We Care Staffing Solutions’ disability support services are available to anyone with a disability and in need of extra support to continue living at home and enjoying the things they love. 
              </p>
              <p>
                Whether it’s help to get your day started, weekly support to do your shopping, or help with getting to appointments, we can tailor our in-home services to meet your needs. Our services are flexible and affordable. We work with your care plan to deliver the services you need, whether that’s one-off support or ongoing help.
              </p>
              <p>
                With more than eight years’ experience as an in-home care provider, we know how important it is to be supported to continue to live at home safely and comfortably. Our team will get to know you and your interests and goals to ensure we deliver the best services and supports to help you achieve those goals.
              </p>
              <p>
                We Care supports a range of clients under a variety of programs and schemes, including the NDIS, CHSP, WorkSafe, and TAC.
              </p>
            </div>

                <div class="tac_image">
                    <img src="assets/images/communitycare_main.png" alt="">

                    <div class="sub_image">
                        <img src="assets/images/communitycare_subimg.png" alt="">
                    </div>

                </div>
           </div>
           <div class="social_community">
            <div class="contents">
                <h3>
                    Social & Community
                </h3>
                <p>
                    We believe that everyone can achieve their goals and fulfilling their different needs and
priorities. Whether you are looking for ways to stay active, connect with others, or simply
enjoy the little things in life, we are here to help. No matter what you want to accomplish,
we’re confident that we can help you reach your goals. So don’t hesitate, let us know and
we’ll get started right away!
                </p>
                <h5>
                    Call us now on 1300 202 265 or 
                </h5>
                <h5>
                    “Complete the NDIS
Referral form now”
                </h5>
                <div class="referal_btn">
                    <button class="primary_btn">
                        Referral Form
                    </button>
                </div>
            </div>
            <div class="image">
                <img src="assets/images/social_media.png" alt="">
            </div>
           </div>
        </div>
    </div>



  </div>






</main>

<?php include 'master/footer.php' ?>

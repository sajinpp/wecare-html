<?php include "master/header.php" ?>

<main>
  <div class="contact_banner team_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Join Our Team 
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Join Our Team </li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="team_page">
    <div class="whychoose_team">
        <div class="whychoose_contents">
            <h3>
                Why Choose WCSS
            </h3>
            <p>
                Healthcare is one of the fastest growing sectors in Australia, and you can be part of it.
At We Care, we are always looking for great people from diverse backgrounds to join our team.
If you’re interested in giving back to the community, have a strong work ethic, and can deliver quality services with respect and a positive attitude, we want to hear from you. We have opportunities for registered nurses, endorsed enrolled nurses, assistants in nursing, personal care assistants, disability support workers, kitchen assistants, domestic assistants, and CSSD technicians.
We understand that providing the highest quality care to our clients means not only recruiting, but also keeping, the highest quality employees. This means that we work around your availability and pay above award wages. We also offer a range of training and development opportunities, interstate travel opportunities, and an employee recognition scheme.
            </p>
        </div>
        <div class="whychoose_image">
            <img src="assets/images/icons/whychoose_team.png" alt="">
        </div>
    </div>

    <div class="staff_outer">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="staff_testimonials">
                        <h2>
                            Staff Video Testimonials
                        </h2>
  
                        <div class="team_swiperWrapper">
                            <div class="swiper Teamslider">
                                <div class="swiper-wrapper">
                                  <div class="swiper-slide">
                                    <div class="team_iframewrapper">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Vblvb_s12zM?si=YSDNl7xj7KfZ1LiG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                    <div class="team_iframewrapper">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Vblvb_s12zM?si=YSDNl7xj7KfZ1LiG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                    </div>
                                  </div>
                                  <div class="swiper-slide">
                                    <div class="team_iframewrapper">
                                        <iframe width="560" height="315" src="https://www.youtube.com/embed/Vblvb_s12zM?si=YSDNl7xj7KfZ1LiG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="swiper-button-next" id="testimonial_next">
                                    
                                <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18.7778 14.3333L25.4444 21L18.7778 27.6667M41 21C41 9.95431 32.0457 1 21 1C9.95431 1 1 9.95431 1 21C1 32.0457 9.95431 41 21 41C32.0457 41 41 32.0457 41 21Z" stroke="#087B87" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>
                                                                      
                            </div>
                            <div class="swiper-button-prev" id="testimonial_prev">
                               
                                <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M23.2222 14.3333L16.5556 21L23.2222 27.6667M0.999998 21C0.999998 9.95431 9.9543 1 21 1C32.0457 1 41 9.95431 41 21C41 32.0457 32.0457 41 21 41C9.9543 41 0.999998 32.0457 0.999998 21Z" stroke="#087B87" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                                </svg>  

                            </div>
                        </div>
                    </div>

                    <div class="job_typewrapper">
                        <h2>
                            Types of Jobs Available
                        </h2>
                        <p>
                            At We Care Staffing Solutions, we’re all about working in with your availability and career goals. We have a range of work opportunities available to suit your needs as a health care worker:
                        </p>

                        <div class="type_boxwrapper">
                            <div class="type_box">
                                <div class="job_image">
                                    <img src="assets/images/jobtype_image.png" alt="">
                                </div>
                                <div class="job_details">
                                    <h3>
                                        Casual work
                                    </h3>
                                    <p>
                                        We offer a variety of casual shifts including weekdays, nights, weekends and public holidays, giving you the choice of when you want to work.
                                    </p>
                                    <button class="primary_btn" data-bs-toggle="modal" data-bs-target="#resume_upload">
                                        Apply now 
                                    </button>
    
                                </div>
                            </div>
                            <div class="type_box typebox_second">
                                <div class="job_details">
                                    <h3>
                                        Short-term contracts 
                                    </h3>
                                    <p>
                                        Short-term contracts are usually for temporary jobs, ranging from a few weeks to several months. They can be for part-time or full-time hours.
                                    </p>
                                    <button class="primary_btn" data-bs-toggle="modal" data-bs-target="#resume_upload">
                                        Apply now 
                                    </button>
                                </div>
                                <div class="job_image">
                                    <img src="assets/images/type_2.png" alt="">
                                </div>
                            </div>
                            <div class="type_box">
                                <div class="job_image">
                                    <img src="assets/images/type_3.png" alt="">
                                </div>
                                <div class="job_details">
                                    <h3>
                                        Long-term contracts 
                                    </h3>
                                    <p>
                                        These are the same as short-term contracts but the duration of the contract is longer, usually between six months to 12 months at a time. Like short-term contracts, they can be for part-time or full time hours.
                                    </p>
                                    <button class="primary_btn" data-bs-toggle="modal" data-bs-target="#resume_upload">
                                        Apply now 
                                    </button>
    
                                </div>
                            </div>
                            <div class="type_box typebox_second">
                                <div class="job_details">
                                    <h3>
                                        Permanent roles
                                    </h3>
                                    <p>
                                        Permanent jobs can be either full-time or part-time hours, where you will be recruited for the client directly.
                                    </p>
                                    <button class="primary_btn" data-bs-toggle="modal" data-bs-target="#resume_upload">
                                        Apply now 
                                    </button>
                                </div>
                                <div class="job_image">
                                    <img src="assets/images/type_4.png" alt="">
                                </div>
                            </div>
                            <div class="type_box">
                                <div class="job_image">
                                    <img src="assets/images/type_5.png" alt="">
                                </div>
                                <div class="job_details">
                                    <h3>
                                        Regional & rural nursing opportunities
                                    </h3>
                                    <p>
                                        Regional and rural opportunities are those roles located outside major metropolitan centres. This can include regional hubs, smaller rural towns, farming communities, and remote settlements. In these roles, accommodation and meals are provided.
                                    </p>
                                    <button class="primary_btn" data-bs-toggle="modal" data-bs-target="#resume_upload">
                                        Apply now 
                                    </button>
    
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>

  </div>






</main>

<?php include 'master/footer.php' ?>

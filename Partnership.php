<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Partnering with We Care
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">NDIS Provider Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="offer_section">
    <div class="particle">
        <img src="assets/images/partnership_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="offer_details">
                    <div class="offer_contents">
                        <h3>
                            The Offer
                        </h3>
                        <p>
                             We believe that the best and most consistent way for us to expand our business by way of investment partnerships with individuals looking at investment into business. 
                        </p>
                        <p>
                            We have mapped out clearly defined territories based on census data.
                        </p>
                        <p>
                            Available territories can be found at: WCSS Available Investment Territories - Google My Maps
                        </p>
                        <p>
                            Each territory will have its own pre-determined valuation. Based on those valuations, we are seeking individuals looking to invest a minimum of $150K and the investment amount against the predetermined valuation will determine the partnership split. 
                        </p>
                        <p>
                            For example, if the valuation is $550,000 and the investment amount is $250,000, the investor will be entitled to 45% of the business and profits out of that Territory. Full profit and loss statements for territories will be distributed and profits paid quarterly. 
                        </p>
                    </div>
                       <div class="offer_imagewrapper">
                            <div class="offerimg1">
                                <img src="assets/images/partner_img.png" alt="">
                            </div>
                            <div class="offerimg1 offerimg2">
                                <img src="assets/images/partner_img2.png" alt="">
                            </div>
                       </div>
                    
                </div>

                <div class="partner_details">
                    <p>
                        Apart from capital investment, the successful investor will bring the following to the table:
                    </p>
                    <ul>
                        <li>
                            Must be able to devote their full-time energies to the role. As such they will be employed by the business and draw a salary of at least $75K p/a. We want them as part of the team and the face of WCSS for that territory. 
                        </li>
                        <li>
                            Need not have industry experience, although it is advantageous.
                        </li>
                        <li>
                            Need no formal healthcare qualifications, as we have all these ourselves.
                        </li>
                        <li>
                            Have good presentation and personable skills. The key role of the employee investor will be managing the relationships with staff (recruitment and retention) and clients (client liaison).
                        </li>
                    </ul>
                    <p>
                        What WCSS will bring to the table:
                    </p>

                    <div class="wcss_image">
                        <img src="assets/images/wcss_table.png" alt="">
                    </div>

                </div>

                <div class="territory_list">
                    <h2>
                        Available Territories
                    </h2>
                    <p>
                        We have mapped out clearly defined territories based on the last Australian Census data of 2021.
                    </p>

                    <div class="territory_img">
                        <img src="assets/images/territories.png" alt="">
                    </div>

                    <p>
                        Each Territory has key statistics we believe are good indicators of a strong health care business proposition. 
                    </p>

                    <div class="territory_statistics">
                        <img src="assets/images/territory_statistics.png" alt="">
                    </div>

                    <p>
                        These are key indicators such as:
                    </p>

                    <ul>
                        <li>
                            Total Population within that territory
                        </li>
                        <li>
                            Number of Hospitals
                        </li>
                        <li>
                            Number of residential aged acre facilities
                        </li>
                    </ul>

                    <p>
                        We release territories only when we are ready and in line with our expansion plans. At any given time, you can see the available territories we have on offer at:
                    </p>

                    <p>
                        WCSS Available Investment Territories - Google My Maps
                    </p>

                    <p>
                        You will also see the Minimum Investment Amount required for that specific territory.
                    </p>

                </div>
            </div>
        </div>
    </div>

    <div class="pathway_section">

        <div class="pathway_particle">
            <img src="assets/images/pathway_particle.png" alt="">
        </div>

         <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2>
                        Pathway to Partnership
                    </h2>
                    <div class="pathway_img">
                        <img src="assets/images/pathway.png" alt="">
                    </div>
                </div>
            </div>
         </div>           
    </div>

  </div>






</main>

<?php include 'master/footer.php' ?>


<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="footer_wrapper">
                    <div class="logo">
                        <a href="">
                            <img src="assets/images/icons/footerlogo.svg" alt="">
                        </a>
                    </div>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
                    </p>
                    <ul class="social_media">
                        <div class="icon">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12 2C6.489 2 2 6.489 2 12C2 17.511 6.489 22 12 22C17.511 22 22 17.511 22 12C22 6.489 17.511 2 12 2ZM12 4C16.4301 4 20 7.56988 20 12C20 16.0145 17.0653 19.313 13.2188 19.8984V14.3848H15.5469L15.9121 12.0195H13.2188V10.7266C13.2188 9.74356 13.539 8.87109 14.459 8.87109H15.9355V6.80664C15.6755 6.77164 15.1268 6.69531 14.0898 6.69531C11.9238 6.69531 10.6543 7.83931 10.6543 10.4453V12.0195H8.42773V14.3848H10.6543V19.8789C6.87029 19.2408 4 15.9702 4 12C4 7.56988 7.56988 4 12 4Z" fill="white"/>
                            </svg>                                
                        </div>
                        <div class="icon">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M2.36719 3L9.46289 13.1406L2.74023 21H5.38086L10.6445 14.8301L14.9609 21H21.8711L14.4492 10.375L20.7402 3H18.1406L13.2715 8.6875L9.29883 3H2.36719ZM6.20703 5H8.25586L18.0332 19H16.002L6.20703 5Z" fill="white"/>
                            </svg>                      
                        </div>
                        <div class="icon">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8 3C5.243 3 3 5.243 3 8V16C3 18.757 5.243 21 8 21H16C18.757 21 21 18.757 21 16V8C21 5.243 18.757 3 16 3H8ZM8 5H16C17.654 5 19 6.346 19 8V16C19 17.654 17.654 19 16 19H8C6.346 19 5 17.654 5 16V8C5 6.346 6.346 5 8 5ZM17 6C16.7348 6 16.4804 6.10536 16.2929 6.29289C16.1054 6.48043 16 6.73478 16 7C16 7.26522 16.1054 7.51957 16.2929 7.70711C16.4804 7.89464 16.7348 8 17 8C17.2652 8 17.5196 7.89464 17.7071 7.70711C17.8946 7.51957 18 7.26522 18 7C18 6.73478 17.8946 6.48043 17.7071 6.29289C17.5196 6.10536 17.2652 6 17 6ZM12 7C9.243 7 7 9.243 7 12C7 14.757 9.243 17 12 17C14.757 17 17 14.757 17 12C17 9.243 14.757 7 12 7ZM12 9C13.654 9 15 10.346 15 12C15 13.654 13.654 15 12 15C10.346 15 9 13.654 9 12C9 10.346 10.346 9 12 9Z" fill="white"/>
                            </svg>                      
                        </div>
                    </ul>

                    <ul class="footer_menu">
                        <li><a href="">
                            About Us
                        </a></li>
                        <li><a href="">
                            Services
                        </a></li>
                        <li><a href="">
                            NDIS & Disability Services
                        </a></li>
                        <li><a href="">
                            Join Our Team
                        </a></li>
                        <li><a href="Blog.php">
                            Blog
                        </a></li>
                        <li><a href="">
                            Partnership
                        </a></li>
                        <li><a href="Testimonials.php">
                            Testimonials
                        </a></li>
                        <li><a href="contact.php">
                            Contact Us
                        </a></li>
                    </ul>

                    <div class="terms_wrapper">
                        <div class="copyright">
                            Copyright © 2024. Wecare staffing online. All rights reserved.
                        </div>
                        <ul>
                            <li><a href="">
                                Terms & Conditions
                            </a></li>
                            <li><a href="">
                                Privacy Policy
                            </a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>




<script src="./assets/plugins/master.js"></script>
<script src="./assets/app/app.js"></script>
</body>

</html>
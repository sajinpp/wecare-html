<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="./assets/fonts/stylesheet.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="./assets/css/style.css" />
  <link rel="stylesheet" href="./assets/css/style.css" />
  <link rel="icon" type="image/png" sizes="32x32" href="./assets/images/icons/Favicon.png" />
  <title>WeCare</title>
</head>

<body>
  <header class="header">
    <div class="page_header">

      <div class="top_headerWrapper">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <div class="top_header">
                <div class="provider">
                  <div class="provider_img">
                    <img src="assets/images/icons/providerIcon.svg" alt="">
                  </div>
                  <h5>
                    ID 4-G4D7YL3
                  </h5>
                </div>
                <ul>
                  <li>
                    <div class="mail">
                      <div class="email_icon">
                        <img src="assets/images/icons/mail.svg" alt="">
                      </div>
                      <div class="email_details">
                        <h6>
                          Email
                        </h6>
                        <a class="word_break" href="">info@wecarestaffingonline.com.au </a>
                      </div>
                    </div>
                  </li>
                  <li class="registerbtn_li">
                    <div class="mail">
                      <div class="email_icon">
                        <img src="assets/images/icons/phone.svg" alt="">
                      </div>
                      <div class="email_details">
                        <h6> 
                          Phone
                        </h6>
                        <a href="">1300 202 265</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container">
        <div class="row">
          <div class="col-12">
              <div class="header_wrapper">
                <div class="logo">
                  <a href="index.php">
                    <img src="assets/images/icons/WCSS-Logo.svg" alt="">
                  </a>
                </div>
                <div class="contact_detailwrapper">
                  <div class="contact_details">
                    <ul>
                      <li>
                        <div class="provider_img">
                          <img src="assets/images/icons/providerIcon.svg" alt="">
                        </div>
                        <h5>
                          ID 4-G4D7YL3
                        </h5>
                      </li>
                      <li>
                        <div class="mail">
                          <div class="email_icon">
                            <img src="assets/images/icons/mail.svg" alt="">
                          </div>
                          <div class="email_details">
                            <h6>
                              Email
                            </h6>
                            <a href="">info@wecarestaffingonline.com.au </a>
                          </div>
                        </div>
                      </li>
                      <li class="registerbtn_li">
                        <div class="mail">
                          <div class="email_icon">
                            <img src="assets/images/icons/phone.svg" alt="">
                          </div>
                          <div class="email_details">
                            <h6> 
                              Phone
                            </h6>
                            <a href="">1300 202 265</a>
                          </div>
                        </div>
                        <button class="login_btn primary_btn">
                          <span>Register / Login</span>
                          <img class="profile_icon" src="assets/images/profile.svg" alt="">
                        </button>
                      </li>
                    </ul>
                  </div>
                  <div class="menubar">
                    <ul>
                      <li class="about_category">
                        <a href="about.php">
                          About Us            
                        </a>
                        <svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M11 1L6 6L1 1" stroke="#858585" stroke-width="1.42857" stroke-linecap="round" stroke-linejoin="round"/>
                          </svg> 

                          <div class="service_box" id="about_box">
                            <ul>
                              <li><a href="#because_wecare">Because We care</a></li>
                              <li><a href="#company_overview">Company Overview</a></li>
                              <li><a href="#mission_vision">Mission, Vision and Values</a></li>
                              <li><a href="#history_our">Our History</a></li>
                              <li><a href="#team_members">Meet Experience Team Member</a></li>
                              <li><a href="#family_care">Our Family Caring for yours:</a></li>
                              <li><a href="#training_develop">Training and Development</a></li>
                              <li><a href="#growth_expansion">Growth and Expansion</a></li>
                            </ul>
                          </div>

                      </li>
                      <li class="agency_servicebtn">
                        <a href="agencyService.php">
                          Agency Services
                        </a>
                        <svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M11 1L6 6L1 1" stroke="#858585" stroke-width="1.42857" stroke-linecap="round" stroke-linejoin="round"/>
                          </svg>  

                          <div class="service_box" id="agency_servicebox">
                            <ul>
                              <li><a href="">Emergency/Casual Solutions</a></li>
                              <li><a href="">Short Term Solutions</a></li>
                              <li><a href="">Service Promise</a></li>
                              <li><a href="">Brokerage Services</a></li>
                            </ul>
                          </div>

                      </li>
                      <li class="service_category"> 
                        <a href="disabilityServices.php">
                          NDIS & Disability Services
                        </a>
                        <svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M11 1L6 6L1 1" stroke="#858585" stroke-width="1.42857" stroke-linecap="round" stroke-linejoin="round"/>
                          </svg>  
                          <div class="service_box" id="service_box">
                            <ul>
                              <li><a href="HomeCare.php">In Home Care</a></li>
                              <li><a href="accomodation.php">Accomodation</a></li>
                              <li><a href="CommunityCare.php">Home & Community Care Services</a></li>
                              <li><a href="tac-cllients.php">Work Safe & TAC Clients</a></li>
                              <li><a href="vhc-sevice.php">VHC</a></li>
                              <li><a href="privateCare.php">Private Care Services</a></li>
                            </ul>
                          </div>
                      </li>
                      <li class="ourteam_box_category">
                        <a href="OurTeam.php">
                          Join Our Team
                        </a>
                        <svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M11 1L6 6L1 1" stroke="#858585" stroke-width="1.42857" stroke-linecap="round" stroke-linejoin="round"/>
                          </svg> 
                          <div class="service_box" id="ourteam_box">
                            <ul>
                              <li><a href="">Why Choose WCSS</a></li>
                              <li><a href="">Video Testimonials</a></li>
                              <li><a href="">Types of Jobs Available</a></li>
                            </ul>
                          </div> 
                      </li>
                      <li class="partner_category">
                        <a href="Partnership.php">
                          Partnership
                        </a>
                        <svg width="12" height="7" viewBox="0 0 12 7" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M11 1L6 6L1 1" stroke="#858585" stroke-width="1.42857" stroke-linecap="round" stroke-linejoin="round"/>
                          </svg>  
                          <div class="service_box" id="partner_box">
                            <ul>
                              <li><a href="">The Offer</a></li>
                              <li><a href="">Available Territories</a></li>
                              <li><a href="">Pathway to Partnership</a></li>
                            </ul>
                          </div> 
                      </li>
                      <li>
                        <a href="Blog.php">
                          Blog
                        </a>
                       
                      </li>
                      <li>
                        <a href="contact.php">
                          Contact Us
                        </a>
                       
                      </li>
                    </ul>

                    <div class="social_media">
                      <a href="" class="icons">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M12 2C6.489 2 2 6.489 2 12C2 17.511 6.489 22 12 22C17.511 22 22 17.511 22 12C22 6.489 17.511 2 12 2ZM12 4C16.4301 4 20 7.56988 20 12C20 16.0145 17.0653 19.313 13.2188 19.8984V14.3848H15.5469L15.9121 12.0195H13.2188V10.7266C13.2188 9.74356 13.539 8.87109 14.459 8.87109H15.9355V6.80664C15.6755 6.77164 15.1268 6.69531 14.0898 6.69531C11.9238 6.69531 10.6543 7.83931 10.6543 10.4453V12.0195H8.42773V14.3848H10.6543V19.8789C6.87029 19.2408 4 15.9702 4 12C4 7.56988 7.56988 4 12 4Z" fill="#087B87"/>
                        </svg>                          
                      </a>
                      <a href="" class="icons">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M2.36719 3L9.46289 13.1406L2.74023 21H5.38086L10.6445 14.8301L14.9609 21H21.8711L14.4492 10.375L20.7402 3H18.1406L13.2715 8.6875L9.29883 3H2.36719ZM6.20703 5H8.25586L18.0332 19H16.002L6.20703 5Z" fill="#087B87"/>
                        </svg>
                          
                      </a>
                      <a href="" class="icons">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path d="M8 3C5.243 3 3 5.243 3 8V16C3 18.757 5.243 21 8 21H16C18.757 21 21 18.757 21 16V8C21 5.243 18.757 3 16 3H8ZM8 5H16C17.654 5 19 6.346 19 8V16C19 17.654 17.654 19 16 19H8C6.346 19 5 17.654 5 16V8C5 6.346 6.346 5 8 5ZM17 6C16.7348 6 16.4804 6.10536 16.2929 6.29289C16.1054 6.48043 16 6.73478 16 7C16 7.26522 16.1054 7.51957 16.2929 7.70711C16.4804 7.89464 16.7348 8 17 8C17.2652 8 17.5196 7.89464 17.7071 7.70711C17.8946 7.51957 18 7.26522 18 7C18 6.73478 17.8946 6.48043 17.7071 6.29289C17.5196 6.10536 17.2652 6 17 6ZM12 7C9.243 7 7 9.243 7 12C7 14.757 9.243 17 12 17C14.757 17 17 14.757 17 12C17 9.243 14.757 7 12 7ZM12 9C13.654 9 15 10.346 15 12C15 13.654 13.654 15 12 15C10.346 15 9 13.654 9 12C9 10.346 10.346 9 12 9Z" fill="#087B87"/>
                        </svg>
                          
                      </a>
                    </div>

                  </div>
                </div>

                <div class="mobile_menu">
                  <div class="toggle">
                    <span class="line-toggle"></span>
                    <span class="line-toggle"></span>
                    <span class="line-toggle"></span>
                  </div>
                  <button class="login_btn primary_btn">
                    <span>Register / Login</span>
                    <img class="profile_icon" src="assets/images/profile.svg" alt="">
                  </button>
                </div>

              </div>
          </div>
        </div>
      </div>
    </div>
  </header>



<!-- Modal -->
<div class="modal fade" id="resume_upload" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-body">
          <div class="apply_head">
            <h2>
              Apply Now
            </h2>

            <div class="close_img" data-bs-dismiss="modal" aria-label="Close">
                <img src="assets/images/icons/Close.svg" alt="">
            </div>

          </div>

          <div class="apply_form">
            <form action="">
              <div class="input_wrapper">
                <div class="input_parent">
                  <input type="text" placeholder="Enter Full Name">
                </div>
                <div class="input_parent">
                  <input type="text" placeholder="Enter Contact Number">
                </div>
              </div>
              <div class="input_wrapper">
                <div class="input_parent">
                  <input type="text" placeholder="Enter Email Id">
                </div>
                <div class="input_parent">
                  <select name="" id="">
                      <option value="">
                        Posts Applying for
                      </option>
                      <option value="">
                        Posts Applying for
                      </option>
                      <option value="">
                        Posts Applying for
                      </option>
                  </select>
                </div>
              </div>
              <div class="resume_uploadBox">
                <img src="assets/images/icons/Cloud_Upload.svg" alt="">
                <h6>
                  Upload Resume
                </h6>
              </div>

              <div class="submit_btn">
                <button class="primary_btn">
                  submit 
                </button>
              </div>

            </form>
          </div>
      </div>
    </div>
  </div>
</div>


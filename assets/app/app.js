

$(document).ready(function () {

  // niceselect js

$("select").niceSelect();


  var banner = new Swiper(".banner_slider", {
    speed:1200,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
  });

  var testimonials = new Swiper(".testimonial_swiper", {
    slidesPerView: 2.3,
    spaceBetween: 30,
    speed:1200,

    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },

      568: {
        slidesPerView: 1.5,
        spaceBetween: 10,
      },

      991: {
        slidesPerView: 2,
        spaceBetween: 10,
      },

      1200: {
        slidesPerView: 2.5,
        spaceBetween: 30,
      },


    },
  });


  var client_slide = new Swiper(".client_slider", {
    slidesPerView: 4.5,
    spaceBetween: 60,
    speed:1200,

    breakpoints: {
      320: {
        slidesPerView: 2.3,
        spaceBetween: 20,
      },
      568: {
        slidesPerView: 3,
        spaceBetween: 30,
      },

      768: {
        slidesPerView: 3.5,
        spaceBetween: 30,
      },

      991: {
        slidesPerView: 4,
        spaceBetween: 50,
      },

      1200: {
        slidesPerView: 4.5,
        spaceBetween: 60,
      },
      1600: {
        slidesPerView: 5.5,
        spaceBetween: 60,
      },
      1800: {
        slidesPerView: 6.5,
        spaceBetween: 60,
      },
    },



  });


  let menutoggle = document.querySelector(".toggle");
  let mobilenav = document.querySelector(".menubar");
  let body = document.querySelector("body");
  let main = document.querySelector("main");
  
  menutoggle.onclick = function () {
    menutoggle.classList.toggle("active");
    mobilenav.classList.toggle("active_menu");
    body.classList.toggle("overflow-hidden");
    main.classList.toggle("main_newclass");
  };



  
  let service_category = document.querySelector(".service_category");
  let service_box = document.querySelector("#service_box");
  let about_category = document.querySelector(".about_category");
  let about_box = document.querySelector("#about_box");
  let agency_servicebtn = document.querySelector(".agency_servicebtn");
  let agency_servicebox = document.querySelector("#agency_servicebox");
  let ourteam_box_category = document.querySelector(".ourteam_box_category");
  let ourteam_box = document.querySelector("#ourteam_box");
  let partner_category = document.querySelector(".partner_category");
  let partner_box = document.querySelector("#partner_box");
  
  service_category.onclick = function () {
    service_box.classList.toggle("active");
  };
  about_category.onclick = function () {
    about_box.classList.toggle("active");
  };
  agency_servicebtn.onclick = function () {
    agency_servicebox.classList.toggle("active");
  };
  ourteam_box_category.onclick = function () {
    ourteam_box.classList.toggle("active");
  };
  partner_category.onclick = function () {
    partner_box.classList.toggle("active");
  };



  document.addEventListener('click', function(event) {
    if (!service_box.contains(event.target) && !service_category.contains(event.target)) {
      service_box.classList.remove("active");
    }
    if (!about_box.contains(event.target) && !about_category.contains(event.target)) {
      about_box.classList.remove("active");
    }
    if (!agency_servicebox.contains(event.target) && !agency_servicebtn.contains(event.target)) {
      agency_servicebox.classList.remove("active");
    }
    if (!ourteam_box.contains(event.target) && !ourteam_box_category.contains(event.target)) {
      ourteam_box.classList.remove("active");
    }
    if (!partner_box.contains(event.target) && !partner_category.contains(event.target)) {
      partner_box.classList.remove("active");
    }
  });


  var Teamslider = new Swiper(".Teamslider", {
    slidesPerView: 2,
    spaceBetween: 40,
    speed:1200,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },

    breakpoints: {
      320: {
        slidesPerView: 1,
        spaceBetween: 10,
      },

      568: {
        slidesPerView: 1.5,
        spaceBetween: 10,
      },

      768: {
        slidesPerView: 2,
        spaceBetween: 20,
      },
      1200: {
        slidesPerView: 2,
        spaceBetween: 40,
      },
    },

  });

});
  
  
  
  
 
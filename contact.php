<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Contact Us
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="get_touchwrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="contact_details">
                    <h2>
                        Get in Touch
                    </h2>

                    <div class="contact_boxwrapper">
                        <div class="contact_box">
                            <div class="contact_icon">
                                <img src="assets/images/icons/email.svg" alt="">
                            </div>
                            <div class="contact_number">
                                <label for="">email </label>
                                <h5 class="wrapp_text">
                                    info@wecarestaffingonline.com.au
                                </h5>
                            </div>
                        </div>
                        <div class="contact_box">
                            <div class="contact_icon">
                                <img src="assets/images/icons/call.svg" alt="">
                            </div>
                            <div class="contact_number">
                                <label for="">phone number  </label>
                                <h5>
                                    1300 202 265
                                </h5>
                            </div>
                        </div>
                    </div>

                    <div class="contactform_wrapper">
                        <div class="contact_description">
                            <h3>
                                Connect with us
                            </h3>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt .
                            </p>
                            <div class="form_banner">
                                <img src="assets/images/form_banner.png" alt="">
                            </div>
                        </div>
                        <div class="contact_form">
                            <form action="">
                                <div class="input_parent">
                                    <input type="text" placeholder="Full Name">
                                </div>
                                <div class="input_parent">
                                    <input type="text" placeholder="Email ID">
                                </div>
                                <div class="input_parent">
                                    <input type="text" placeholder="Phone Number">
                                </div>
                                <div class="input_parent">
                                    <textarea name="" id="" placeholder="message">

                                    </textarea>
                                </div>
                                <div class="send_btnwrapper">
                                    <button class="primary_btn">
                                        send message 
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="map_wrapper">
    <img src="assets/images/map.png" alt="">
  </div>

</main>

<?php include 'master/footer.php' ?>

<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                Work Safe and TAC Clients
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Work Safe and TAC Clients</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="tac_clients">
    <div class="particle">
        <img src="assets/images/vhc_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
           <div class="col-12 tac_contentWrapper">

            <div class="tac_details">
                <p>
                    WorkSafe funding is normally given to a person who has been injured or has had an accident in their workplace or during work.
                </p>
                <p>
                    As an approved service provider with WorkSafe and other insurance agencies, We Care Staffing Solutions can provide any service that WorkSafe initiates.  This can include personal care, domestic assistance, or community assistance.
                </p>
                <p>
                    A TAC claim is an insurance claim someone makes if they are injured in a transport accident. It is a ‘no fault’ insurance scheme owned by the Victorian Government. This means that if you are injured in a transport accident in Victoria you can make a TAC claim to help pay for the treatment and support you need, even if the accident was your fault.
                </p>
                <p>
                    When Victorian motorists pay their vehicle registration, they also pay an insurance premium called a ‘TAC charge’. This money is used to support people injured in transport accidents.
                </p>
                <p>
                    If we accept your claim, we can help pay for medical and other expenses related to your accident injuries.
                </p>
            </div>

                <div class="tac_image">
                    <img src="assets/images/tac_clientImg.png" alt="">
                </div>
           </div>
        </div>
    </div>



  </div>






</main>

<?php include 'master/footer.php' ?>

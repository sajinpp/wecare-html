<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                     Provider Services
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Provider Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="Homecare">
    <div class="particle">
        <img src="assets/images/vhc_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
           <div class="col-12 homewrapper">

            <div class="tac_details">
              <p>
                The National Disability Insurance Scheme (NDIS) provides funding and support for people with a disability. Designed to provide greater choice, independence and control, funding is allocated to individuals following an assessment.
              </p>
             <p>
                Eligible NDIS participants must:
             </p>
             <ul>
                <li>Live in a region where the NDIS is available.</li>
                <li>Meet the age and residency requirements.</li>
                <li>Meet either the disability or early intervention requirements.</li>
             </ul>
             <p>
                As an NDIS participant, you choose where to go to access services. When you choose, We Care Staffing Solutions for part or all your services, we work within your NDIS plan to best meet your individual goals and objectives.
             </p>
             <p>
                Remember, you have the right to be safe and to receive quality services from the providers and workers you choose to support you under the NDIS. The NDIS Quality and Safeguards Commission is an independent agency established to improve the quality and safety of NDIS supports and services.
             </p>
             <p>
                At We Care Staffing Solutions we believe it should be easy for you to access a suitable provider and receive the right care for your needs. That’s why we focus on delivering a great service and being a provider that you’ll be happy to stay with. We back our service so much that we won’t charge you exit fees or upgrading fees and we won’t ever lock you in to any price structure.
             </p>
             <p>
                We tailor all services to best meet your ongoing needs and goals. We are a quality-focused and client-focused provider, and we are guided by evidence-based research, continuous improvement, and national and international best practice to provide all participants with the highest quality service and support to meet their individual and ongoing requirements.
             </p>
             <h5>
                Contact us today to see how We Care Staffing Solutions can support you to stay living independently at home with more time to enjoy your
favourite activities.
             </h5>
             <p>
                We Care Staffing Solutions is registered to supply the following NDIS core support services:
             </p>

             <div class="homecare_imagewrapper">
                <div class="contents">
                    <h3>
                        Assistance in Coordinating or Managing Life Stages, Transitions and Supports
                    </h3>
                    <p>
                        Includes plan implementation, connection with broader systems of supports, assistance to participants in designing and building their own supports, and developing resilience in participants’ network.
                    </p>
                    <h3>
                        Assistance with Daily Personal Activities
                    </h3>
                    <p>
                        This is a broad category and covers a range of assistance to help participants develop and maintain independence and quality of life.
                    </p>
                    <h3>
                        Assistance with Travel and Transport Arrangements
                    </h3>
                    <p>
                        Includes specialist transport to educational facilities, employment, and community activities and events, as well as general assistance with transport, transport for shopping, social events, medical appointments, etc.
                    </p>
                </div>

                <div class="homecare_image">
                    <img src="assets/images/homecare_image.png" alt="">
                </div>

             </div>

             <h3>
                Community Nursing Care for High Needs
             </h3>
             <p>
                Care provided by a trained and supervised worker to respond to complex needs.
             </p>
             <h3>
                Assistance with Daily Life Tasks in a Group or Shared Living Arrangement
             </h3>
             <p>
                Assisting with or supervising daily living tasks to develop skills of participants with complex care needs, and help them to live as autonomously as possible within their shared environment.

             </p>
             <h3>
                Innovative Community Participation
             </h3>
             <p>
                Facilitating increased social and community participation and involvement, and (if applicable) employment opportunities.
             </p>
             <h3>
                Development of Daily Living and Life Skills
             </h3>
             <p>
                A broad category which includes public transport training and support and developing skills for social, community, and recreational participation and involvement. Also includes training for carers looking after people with disability. Please also see the information on Supported Independent Living (SIL) below.

             </p>
             <h3>
                Household Tasks
             </h3>
             <p>
                Assistance with a range of household and garden tasks, including cleaning, laundry, and meal preparation.
             </p>

             <h3>
                Participate in Community, Social and Civic Activities

             </h3>
             <p>
                Assisting participants to engage in a range of community, social and recreational activities.
             </p>

             <p>
                We are also registered to deliver the following complex nursing care services:
             </p>

             <ul>
                <li>
                    Enteral (naso-gastric, jejunum or duodenum) feeding and management.
                </li>
                <li>
                    Tracheostomy management
                </li>
                <li>
                    Urinary catheter management (indwelling, in and out, and suprapubic catheters).
                </li>
             </ul>

            </div>
           </div>
        </div>
    </div>



  </div>






</main>

<?php include 'master/footer.php' ?>

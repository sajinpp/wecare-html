<?php include "master/header.php" ?>

<main>
  <div class="contact_banner nids_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
               <div class="nds_bannerwrapper">
                <h2>
                    NDIS Provider Services
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">NDIS Provider Services</li>
                    </ol>
                  </nav>
               </div>
               <div class="refer_form">
                    <h2>
                        Referral form
                    </h2>

                    <div class="input_group">
                        <div class="input_parent">
                            <input type="text" placeholder="Full Name">
                        </div>
                        <div class="input_parent">
                            <input type="text" placeholder="Email ID">
                        </div>
                        <div class="input_parent">
                            <input type="text" placeholder="Phone Number">
                        </div>
                    </div>

                    <div class="complete_btn">
                        <button class="primary_btn">
                            Complete
                        </button>
                    </div>


               </div>
            </div>
        </div>
    </div>
  </div>


  <div class="disability_serviceWrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="home_accomadation">
                    <li><a href="HomeCare.php">
                        <div class="service_img">
                            <img src="assets/images/service1.png" alt="">
                        </div>
                        <div class="content_box">
                            In Home Care
                        </div>
                    </a></li>
                    <li><a href="accomodation.php">
                        <div class="service_img">
                            <img src="assets/images/service2.png" alt="">
                        </div>
                        <div class="content_box">
                            Accomodation
                        </div>
                    </a></li>
                </ul>

                <div class="other_services">
                    <h2>
                        Other Services
                    </h2>
                    <ul class="other_serviceUl">
                        <li><a href="CommunityCare.php">
                            <div class="service_img">
                                <img src="assets/images/service3.png" alt="">
                            </div>
                            <div class="content_box">
                                Home and Community Care Services
                            </div>
                        </a></li>
                        <li><a href="tac-cllients.php">
                            <div class="service_img">
                                <img src="assets/images/service4.png" alt="">
                            </div>
                            <div class="content_box">
                                Work Safe and TAC Clients
                            </div>
                        </a></li>
                        <li><a href="vhc-sevice.php">
                            <div class="service_img">
                                <img src="assets/images/service5.png" alt="">
                            </div>
                            <div class="content_box">
                                VHC
                            </div>
                        </a></li>
                        <li><a href="privateCare.php">
                            <div class="service_img">
                                <img src="assets/images/service6.png" alt="">
                            </div>
                            <div class="content_box">
                                Private Care Services
                            </div>
                        </a></li>
                    </ul>
                </div>

            </div>
        </div>
    </div>
  </div>

  <div class="disability_whychoose">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="why_choosedetails">
                    <div class="left_choose">
                        <h2>
                            Why Choose Us
                        </h2>
                        <h3>
                            We are experienced carers.
                        </h3>
                        <p>
                            We Care Staffing Solutions has a proven track record of supporting people with disability and older people to live independently at home, improving their quality of life, wellbeing, and health. Once you choose us you will always be in safe and experienced hands and your family members can rest assured that we will take care of you.
                        </p>
                        <h3>
                            We tailor support to your unique needs
and funding.
                        </h3>
                        <p>
                            We Care Staffing Solutions always provides support tailored to your needs and helps you use your funding effectively. Our clients (sometimes called customers or participants) are our priority. We make sure you receive the quality care and services you require, and we work closely with you to review your short-term and long-term goals within your proposed timeframes. 
                        </p>
                        <h3>
                            We can help make your Home Care Package work for you, and your funding go further, so you can get more hours and benefits out of your support.
                        </h3>
                        <p>
                            We understand that you want to use your funding for as much as you can. As our customer, we make sure you receive great value from your funding, with more options and better control.
We value our customers’ freedom and the principle of customer directed care. At We Care Staffing Solutions you are in charge. We provide tailored services to meet your needs and you will always have the independence to use your package as you require.
                        </p>
                    </div>
                    <div class="right_choose">
                        <div class="why_chooseimg">
                            <img src="assets/images/why_choose.png" alt="">
                        </div>

                        <h3>
                            Staff compliance, training and qualifications
                        </h3>

                        <p>
                            Our staff are big-hearted, empathetic, knowledgeable, highly skilled and qualified, and they undertake ongoing training and professional development to ensure they are equipped to provide the highest quality care and to meet your specific ongoing needs.
All our staff have completed the required qualifications, are thoroughly reference checked, criminal history/record checked, and they always carry photographic identification. Our adherence to best practice recruitment and training ensures that we always offer you the best people to provide the best outcomes.
                        </p>

                        <h3>
                            Support 24 hours a day, seven days a week
                        </h3>
                        <p>
                            We are here to support you day and night, including weekends and public holidays. Services can often be arranged within 24 hours and our flexible care means we can support you in an emergency or one-off basis. If you would like a service ‘holiday’, we’re able to put your services on hold until you’re ready to continue.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>






</main>

<?php include 'master/footer.php' ?>

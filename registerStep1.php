<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Apply Now
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">NDIS Provider Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="stepwrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="step_box">
                    <ul class="step_head">
                        <li class="active">
                            1
                        </li>
                        <li>
                            2
                        </li>
                        <li>
                            3
                        </li>
                    </ul>
                    <div class="contents">
                        <h6>You are applying for</h6>
                        <h2>Registered Nurses - Aged Care</h2>
                    </div>

                    <form action="">
                        <h3>
                            Register Now
                        </h3>

                        <div class="input_group">
                            <label for="">Enter your email id</label>
                            <div class="input_parent">
                                <select name="" id="">
                                    <option value="">1</option>
                                    <option value="">2</option>
                                    <option value="">3</option>
                                </select>
                            </div>
                        </div>

                        <div class="next_btn">
                            <a class="primary_btn" href="registerStep2.php">
                                Next
                            </a>
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </div>
  </div>






</main>

<?php include 'master/footer.php' ?>

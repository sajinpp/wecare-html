<?php include "master/header.php" ?>

<main>
  <div class="contact_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    Private Care Services
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="index.php">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">Private Care Services</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="private_carepage">
    <div class="particle">
        <img src="assets/images/vhc_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
           <div class="col-12 private_careWrapper">

            <div class="tac_details">
               <p>
                You do not need government funding or assessments to access our services. We Care Staffing Solutions provides care for older people and people who cannot access funding and want to stay in their own homes.
               </p>
               <p>
                As a private care customer, you might just need that extra help around the house and garden to do the chores you can’t do on your own. You may want to engage with the community, attend functions, get help to regain your mobility, or just do some shopping. Every customer has different goals and preferences and it’s our job to listen to and identify your individual needs and goals. We can provide all the services you need tailored just for you, and you are always in charge.
               </p>
            </div>

                <div class="tac_image">
                    <img src="assets/images/privatecare_main.png" alt="">

                    <div class="sub_image">
                        <img src="assets/images/privatecare_subimg.png" alt="">
                    </div>

                </div>
           </div>
        </div>
    </div>



  </div>






</main>

<?php include 'master/footer.php' ?>

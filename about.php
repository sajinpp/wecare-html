<?php include "master/header.php" ?>

<main>
  <div class="about_banner">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2>
                    About Us
                </h2>
                <nav style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='%236c757d'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                      <li class="breadcrumb-item"><a href="#">Home</a></li>
                      <li class="breadcrumb-item active" aria-current="page">About Us</li>
                    </ol>
                  </nav>
            </div>
        </div>
    </div>
  </div>

  <div class="about_hospital">
    <div class="overview_particle">
        <img src="assets/images/company_overview.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about_imagewrapper" id="because_wecare">
                    <div class="about_image">
                        <img src="assets/images/about_image2.png" alt="">
                    </div>
                    <div class="about_contents">
                        <h6>
                            about Us
                        </h6>
                        <h2>
                            Because We Care
                        </h2>
                        <p>
                            We Care is more than just a name. Our company was founded on care, respect and
compassion – and we apply these in everything we do.
                        </p>
                        <p>
                            From starting as a small staffing agency with a handful of staff in Melbourne, we
have expanded to offer a range of services. These include NDIS and disability
services, veteran home care services, nursing agency services and being an
approved TAC and Work Cover provider. Our staffing solutions encompass casual,
                        </p>
                        <p>
                            short-term or permanent staffing, international recruitment, and home and
community brokerage services. We have also grown by opening offices to serve
clients in New South Wales, the Australian Capital Territory and Queensland.
                        </p>
                    </div>
                </div>
                <p class="author_paragraph">
                    As Australia’s healthcare needs continue to grow, we plan to be at the forefront of
assisting people and organisations to meet their nursing and disability support
needs.
We invite you to learn about our range of services and our approach to ensuring
quality and compassionate care and would love for you to join us in our journey to
become one of Australia’s leading healthcare providers.
                </p>

                <div class="client_content">
                    <div class="client_img">
                        <img src="assets/images/client.png" alt="">
                    </div>
                    <div class="client_details">
                        <h5>
                            Sincerely,
                        </h5>
                        <h6>
                            Anish Pushpanathan
                        </h6>
                    </div>
                </div>

                <div class=" communitycareWrapper"  id="company_overview">

                    <div class="tac_details">
                        <h3>
                            Company Overview
                        </h3>
                        <p>
                            We Care Staffing Solutions (WCSS) was founded in 2012 by Melbourne-based
healthcare professionals. We provide quality healthcare services to individual clients
and emergency staffing assistance to a range of healthcare providers. 
                        </p>
                        <p>
                            The services we offer include:
                        </p>
                        <ul>
                            <li>Disability support services</li>
                            <li>Supported independent living (SIL) services</li>
                            <li> Nursing agency services</li>
                            <li>Permanent recruitment solutions</li>
                            <li>International recruitment for aged care and hospitals</li>
                            <li>Home and community brokerage services</li>
                            <li>Private care services.</li>
                        </ul>
                        <p>
                            With ongoing growth since our founding, we now have over 2,000 staff servicing
more than 300 organisations across Victoria, New South Wales, the Australian
Capital Territory and Queensland. We look forward to serving more people and
providers as we continue to grow.
                        </p>
                    </div>
        
                        <div class="tac_image">
                            <img src="assets/images/communitycare_main.png" alt="">
        
                            <div class="sub_image">
                                <img src="assets/images/communitycare_subimg.png" alt="">
                            </div>
        
                        </div>
                   </div>

            </div>
        </div>
    </div>
  </div>

  <div class="mission_section" id="mission_vision">

    <div class="mission_particle1">
        <img src="assets/images/mission_particle1.png" alt="">
    </div>

    <div class="mission_particle2">
        <img src="assets/images/mission_particle2.png" alt="">
    </div>


    <div class="container">
        <div class="row">
            <div class="col-12">

                <h2>
                    Mission, Vision and Values
                </h2>

                <ul class="mission_boxwrapper">
                    <li>
                        <h3>
                            Vision
                        </h3>
                        <p>
                            Our vision is to be recognised as Australia’s best caregiving professionals and providers.
                        </p>
                    </li>
                    <li>
                        <h3>
                            Mission
                        </h3>
                        <p>
                            Our mission is to provide the highest quality staffing solutions and care to the people
                            who need it most.
                        </p>
                    </li>
                    <li>
                        <h3>
                            Core Values
                        </h3>
                        <p>
                            We do what we say we will. On time, every time.
                        </p>
                    </li>
                    <li>
                        <h3>
                            Quality Care
                        </h3>
                        <p>
                            By hiring top quality candidates and taking the time to match their skills and experience to the needs of our clients, we deliver the very best care tailored to the needs of the individual.
                        </p>
                    </li>
                    <li>
                        <h3>
                            Respect
                        </h3>
                       <p>
                        We hold our customers and staff in the highest regard and treat all people with humanity and dignity.
                       </p>
                    </li>
                    <li>
                        <h3>
                            Reliability
                        </h3>
                      <p>
                        We do what we say we will. On time, every time.
                      </p>
                    </li>
                    <li>
                        <h3>
                            Accountability
                        </h3>
                      <p>
                        We take ownership of our work and honour their commitments to our clients and partner organisations.
                      </p>
                    </li>
                    <li>
                        <h3>
                            Excellence
                        </h3>
                        <p>
                            We are committed to consistently delivering the very best service and outcomes.
                        </p>
                    </li>
                    <li>
                        <h3>
                            Commitment
                        </h3>
                        <p>
                            We dedicate our time to providing a service we are passionate about. Because We Care.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
  </div>

  <section class="history_section" id="history_our">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="history_wrapper">
                    <h2>
                        Our History
                    </h2>
                    <p>
                        Since 2012, we have grown from a small team in Victoria to over 2,000 across
Australia’s eastern states.
                    </p>

                    <div class="timeline_wrapper">
                        <div class="years_vertical">
                           <ul class="year_listing">
                                <li class="active">
                                    <div class="timeline_dots">
                                        <h6>
                                            2012
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <p>
                                            We Care Staffing Solutions was founded by healthcare professionals to
provide labour-hire services to disability and community organisations across
Melbourne. In the beginning, we had four office staff, 80 employees and 20 clients.
                                        </p>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2017
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <p>
                                            Expanded into Canberra, ACT, with our first interstate office providing
                                            healthcare professionals to residential aged-care facilities and hospitals. Our staff
                                            numbers grew to 150, serving 150 clients.
                                        </p>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2018
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <p>
                                            Expansion to Wagga Wagga, New South Wales.
                                        </p>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2019
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <p>
                                            Expansion to Orange, New South Wales.
                                        </p>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2020
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <p>
                                            Received NDIS registration to provide direct care services to clients.
                                        </p>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2022
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <ul>
                                            <li>Opened our first supported independent living (SIL) house in Orange, New
                                                South Wales.</li>
                                            <li>
                                                Became an approved provider for the Department of Veterans Affairs of the
Veterans Home Care Program and began serving clients across VIC, NSW, ACT.
                                            </li>
                                            <li>
                                                Expanded into Albury, New South Wales.
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2023
                                        </h6>
                                        <div class="point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <ul>
                                            <li>Our total number of clients grew to 300 and the number of employees to
                                                2,000.</li>
                                            <li>
                                                Opened our first SIL house in Victoria, bringing the total to 9 homes across
New South Wales and Victoria.
                                            </li>
                                            <li>
                                                Expanded VHC services to Queensland.
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                                <li>
                                    <div class="timeline_dots">
                                        <h6>
                                            2024
                                        </h6>
                                        <div class="point last_point">
                                            <div class="dot"></div>
                                        </div>
                                    </div>

                                    <div class="yearbox">
                                        <ul>
                                            <li>Our total number of clients grew to 300 and the number of employees to
                                                2,000.</li>
                                            <li>
                                                Became an approved provider for the Department of Veterans Affairs of the
Veterans Home Care Program and began serving clients across VIC, NSW, ACT.
                                            </li>
                                            <li>
                                                Expanded into Albury, New South Wales.
                                            </li>
                                        </ul>
                                    </div>

                                </li>
                             
                           </ul>
                        </div>
                       
                    </div>

                </div>
            </div>
        </div>
    </div>
  </section>

  <div class="our_teamsection" id="team_members">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h6>
                    Our Team
                </h6>
                <h2>
                    Meet Experience Team Member
                </h2>

                <ul>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/anish.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Anish Pushpanathan
                            </h3>
                            <h6>
                                Co-Founder and Director of Operations
                            </h6>
                            <p>
                                Anish, the Founder, CEO, and Director of We Care Staffing Solutions, <span data-bs-toggle="modal" data-bs-target="#author_pop">...Read more </span>
                            </p>
                            
                        </div>
                    </li>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/neethu.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Neethu Jose
                            </h3>
                            <h6>
                                Co-Founder and Director of Service
                            </h6>
                            <p>
                                We Care, is a seasoned professional with over 20 years of extensive experience <span>read more </span>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/benjamin.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Benjamin Fernandes
                            </h3>
                            <h6>
                                Corporate Growth Manager
                            </h6>
                           <p>
                            We Care, is a seasoned professional with over 20 years of extensive experience
                        </div>
                    </li>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/tammy.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Tammy Ogaji
                            </h3>
                            <h6>
                                Operations Manager
                            </h6>
                            <p>
                                We Care, is a seasoned professional with over 20 years of extensive experience.
                        </div>
                    </li>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/anish.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Anish Pushpanathan
                            </h3>
                            <h6>
                                Co-Founder and Director of Operations
                            </h6>
                            <p>
                                Anish, the Founder, CEO, and Director of We Care Staffing Solutions, <span>read more </span>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/tammy.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Tammy Ogaji
                            </h3>
                            <h6>
                                Operations Manager
                            </h6>
                            <p>
                                We Care, is a seasoned professional with over 20 years of extensive experience.
                        </div>
                    </li>
                    <li>
                        <div class="member_img">
                            <img src="assets/images/anish.png" alt="">
                        </div>
                        <div class="member_details">
                            <h3>
                                Anish Pushpanathan
                            </h3>
                            <h6>
                                Co-Founder and Director of Operations
                            </h6>
                            <p>
                                Anish, the Founder, CEO, and Director of We Care Staffing Solutions, <span>read more </span>
                            </p>
                        </div>
                    </li>
                </ul>

                

            </div>
        </div>
    </div>
  </div>

  <div class="training_methods">
    <div class="growth_particle1">
        <img src="assets/images/growth_particle1.png" alt="">           
    </div>
    <div class="growth_particle2">
        <img src="assets/images/growth_particle2.png" alt="">           
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class=" communitycareWrapper">

                    <div class="tac_image">
                        <img src="assets/images/about_primary.png" alt="">
    
                        <div class="sub_image">
                            <img src="assets/images/about_secondary2.png" alt="">
                        </div>
    
                    </div>

                    <div class="tac_details" id="family_care">
                        <h3>
                            Our Family Caring for yours:
                        </h3>
                        <p>
                            As a healthcare and disability labour-hire business, our people are the key to our
                            service delivery, customer satisfaction and success. We strongly believe that
                            everyone we work with deserves the best care possible. Our rigorous recruitment
                            and pre-screening process ensures all staff members have the experience,
                            qualifications and attitudes to meet our high standards and deliver client satisfaction.
                        </p>
                        <p>
                            Our recruitment process includes:
                        </p>
                        <ul>
                            <li>Face-to-face and online interviews.</li>
                            <li>Minimum six months of post qualiﬁcation paid experience.</li>
                            <li>Checking employment histories and at least two professionalb references.</li>
                            <li>Conducting checks, including National Police, Working with Children, Working
                                with Vulnerable Children, Work Rights, NDIS, AHPRA Registration for Nurses
                                and Midwives.</li>
                            <li>Requiring Statutory Declarations. </li>
                            <li>Checking immunisation certificates, including for flu and COVID-19.</li>
                        </ul>

                    </div>
        
                       
                </div>
                <div class="training_development" id="training_develop">
                    <h3>
                        Training and Development                     
                    </h3>
                    <div class="training_contents">
                        <div class="contents">
                            <p>
                                At We Care Staffing Solutions, we provide regular training and development to
our staff to ensure health, safety and compliance. This training keeps our staff
up to date in best-practice care techniques, beneﬁting our clients and their
customers, as well as advancing staff in their careers.
                            </p>
                            <p>
                                Some of the face-to-face and online training programs we provide include:
                            </p>
                            <ul>
                                <li>BLS for Registered and Enrolled Nurses</li>
                                <li>Bullying and Harassment</li>
                                <li>Covid-19</li>
                                <li>CPR</li>
                                <li>Elder Abuse Prevention</li>
                                <li>Fall Prevention</li>
                                <li>Fire and Safety</li>
                                <li>Hand Hygiene</li>
                                <li>Infection Control</li>
                                <li>Mandatory Reporting</li>
                                <li>Manual Handing</li>
                                <li>Medication Error Prevention</li>
                                <li>Workplace Health and Safety.  </li>
                            </ul>
                        </div>
                        <div class="image_section">
                            <img src="assets/images/training_image.png" alt="">
                        </div>
                    </div>
                    <div class="training_contents growth_expansion" id="growth_expansion">
                        <div class="image_section">
                            <img src="assets/images/training_image.png" alt="">
                        </div>
                        <div class="contents">
                            <h3>
                                Growth and Expansion
                            </h3>
                            <p>
                                Australia’s aging population and the growth in government spending in this sector
presents an opportunity for WCSS to fill the void in providing staff to assist with
healthcare for older Australians, as well as to look to maintain and secure ongoing
contracts with government agencies.
                            </p>
                            <h4>
                                Opening new premises
                            </h4>

                            <p>
                                With increased demand, we aim to expand geographically and open new premises.
We have mapped out and clearly defined areas for expansion based on key
indicators of a strong healthcare business proposition.
                            </p>

                            <p>
                                These key indicators we consider include the:
                            </p>

                            <ul>
                                <li>Total population within each territory based on Australian census data.</li>
                                <li>Number of hospitals within each territory based on publicly available information.</li>
                                <li>Number of residential aged care facilities based on publicly available information.</li>
                            </ul>
                        </div>
                      
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="software_section">
    <div class="software_particle">
        <img src="assets/images/software_particle.png" alt="">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about_software">

                    <div class="introduction">
                        <h6>Our Software</h6>
                        <h2>
                            You'll Love the Way
We Care for You...
                        </h2>
                        <h5>
                            Introduction
                        </h5>
                        <p>
                            We Care Staffing Solutions operate a software management system that can be accessed on most platforms, including Microsoft Windows, Apple OS, and Android. Our staff and clients can request, track and receive live confirmations of requested shifts.
We Care Staffing Solutions office management staff are also able to login at any time as administrators, to determine whether there are any discrepancies requiring attention. This would automatically flag the system to enable us to immediately act upon any issues that may arise in real time.
All the above will be duly demonstrated and clearly understood by all your relevant staff prior to their commencing with you, and our administrative staff are more than happy to assist you with any enquiries at any time should the need arise.
                        </p>
                    </div>

                    <div class="software_registerform">
                        <h3>
                            Online Services Page
                        </h3>

                        <div class="software_tabs">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                  <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home-tab-pane" type="button" role="tab" aria-controls="home-tab-pane" aria-selected="true">Home</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                  <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile-tab-pane" type="button" role="tab" aria-controls="profile-tab-pane" aria-selected="false">Profile</button>
                                </li>
                              </ul>
                              <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home-tab-pane" role="tabpanel" aria-labelledby="home-tab" tabindex="0">
                                    <p>
                                        Download our app and manage your availability, receive shifts notifications, accept shifts, submit your time sheets , update your details, and much more …..
                                    </p>
                                    <button class="primary_btn" href="">Register / Login</button>
                                </div>
                                <div class="tab-pane fade" id="profile-tab-pane" role="tabpanel" aria-labelledby="profile-tab" tabindex="0">
                                    <p>
                                        Download our app and manage your availability, receive shifts notifications, accept shifts, submit your time sheets , update your details, and much more …..
                                    </p>
                                    <a class="primary_btn" href="">Register / Login</a>
                                </div>
                              </div>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="our_appad">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="ourapp_banner">
                    <div class="phone1">
                        <img src="assets/images/Phone.png" alt="">
                    </div>
                    <div class="contents">
                        <h3>
                            Our APP enables
                        </h3>
                        <ul>
                            <li>Online shifts requests</li>
                            <li>Instant confirmations</li>
                            <li>View Staff information, and previous visits</li>
                            <li>Live chat</li>
                            <li>Online time sheet verification</li>
                        </ul>
                    </div> 
                    <div class="phone">
                        <img src="assets/images/Phone2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="our_clients">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="clients_head">
                    <div class="heading">
                        <h6>
                            our clients
                        </h6>
                        <h2>
                            we work with best partners
                        </h2>
                    </div>

                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam, quis nostrud exercitation ullamco.
                    </p>

                </div>
            </div>
        </div>
    </div>

    <div class="client_sliderwrapper">
        <div class="swiper client_slider">
            <div class="swiper-wrapper">
              <div class="swiper-slide">
                   <img src="assets/images/client1.png" alt="">
              </div>

              <div class="swiper-slide">
                <img src="assets/images/whitecloud.png" alt="">
              </div>
              <div class="swiper-slide">
                <img src="assets/images/lifecell.png" alt="">
              </div>
              <div class="swiper-slide">
                <img src="assets/images/whitecloud.png" alt="">
          </div>

          <div class="swiper-slide">
            <img src="assets/images/landauer.png" alt="">
          </div>
          <div class="swiper-slide">
            <img src="assets/images/lifecell.png" alt="">
          </div>
          <div class="swiper-slide">
            <img src="assets/images/landauer.png" alt="">
      </div>

      <div class="swiper-slide">
        <img src="assets/images/client1.png" alt="">
      </div>
      <div class="swiper-slide">
        <img src="assets/images/whitecloud.png" alt="">
      </div>

            </div>
          </div>
    </div>

  </div>

  <div class="affiliation_section">
    <div class="container">
        <div class="row">
            <div class="col-12 affiliation_wrapper">
                <div class="affiliation_contents">
                    <h6>
                        Affiliations
                    </h6>
                    <h2>
                        We Care Staffing Solutions
                    </h2>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliquat enim ad minim veniam, quis nostrud exercitation ullamco.
                    </p>
                </div>

                <ul>
                    <li>
                        <img src="assets/images/affiliation1.png" alt="">
                    </li>
                    <li>
                        <img src="assets/images/affiliation2.png" alt="">
                    </li>
                    <li>
                        <img src="assets/images/affiliation3.png" alt="">
                    </li>
                    <li>
                        <img src="assets/images/affiliation4.png" alt="">
                    </li>
                    <li>
                        <img src="assets/images/affiliation5.png" alt="">
                    </li>
                    <li>
                        <img src="assets/images/affiliation6.png" alt="">
                    </li>
                </ul>

            </div>
        </div>
    </div>
  </div>

  <div class="client_video_section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="staff_testimonials">
                    <h2>
                        Client Video Testimonials
                    </h2>

                    <div class="team_swiperWrapper">
                        <div class="swiper Teamslider">
                            <div class="swiper-wrapper">
                              <div class="swiper-slide">
                                <div class="team_iframewrapper">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/Vblvb_s12zM?si=YSDNl7xj7KfZ1LiG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="team_iframewrapper">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/Vblvb_s12zM?si=YSDNl7xj7KfZ1LiG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                </div>
                              </div>
                              <div class="swiper-slide">
                                <div class="team_iframewrapper">
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/Vblvb_s12zM?si=YSDNl7xj7KfZ1LiG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="swiper-button-next" id="testimonial_next">
                                
                            <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18.7778 14.3333L25.4444 21L18.7778 27.6667M41 21C41 9.95431 32.0457 1 21 1C9.95431 1 1 9.95431 1 21C1 32.0457 9.95431 41 21 41C32.0457 41 41 32.0457 41 21Z" stroke="#087B87" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                                                                  
                        </div>
                        <div class="swiper-button-prev" id="testimonial_prev">
                           
                            <svg width="42" height="42" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M23.2222 14.3333L16.5556 21L23.2222 27.6667M0.999998 21C0.999998 9.95431 9.9543 1 21 1C32.0457 1 41 9.95431 41 21C41 32.0457 32.0457 41 21 41C9.9543 41 0.999998 32.0457 0.999998 21Z" stroke="#087B87" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>  

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </div>

  <div class="map_wrapper">
    <img src="assets/images/map.png" alt="">
  </div>


</main>



  <!-- Modal -->

  <div class="modal fade modal_author" id="author_pop" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <button type="button" class="btn-close author_close" data-bs-dismiss="modal" aria-label="Close">
            <img src="assets/images/icons/author_close.svg" alt="">
        </button>
        <div class="modal-body">
          <div class="author_wrapper">
            <div class="author_details">
                <div class="author_image">
                    <img src="assets/images/author_anish.png" alt="">
                </div>
                <h6>Anish Pushpanathan</h6>
                <h5>Co-Founder and Director of Operations</h5>
            </div>
            <div class="author_para">
                <p>
                    Anish, the Founder, CEO, and Director of We Care Staffing Solutions, is a seasoned
professional with over 20 years of extensive experience in the healthcare industry.
Beginning his career as a Registered Nurse in critical care and aged care, Anish’s passion for
helping others has remained steadfast.
                </p>
                <p>
                    As an actively involved CEO, Anish takes pride in ensuring that every member of his
organisation shares his unwavering commitment to providing exceptional healthcare
services. Under his leadership, We Care Staffing Solutions has become a trusted provider of
healthcare staffing solutions to many healthcare facilities.
                </p>
                <p>
                    In addition to his vast industry experience, Anish stays abreast of the latest healthcare
trends and innovations, leveraging this knowledge to ensure that We Care Staffing Solutions
continues to offer cutting-edge solutions that meet the evolving needs of the healthcare
industry.
                </p>
                <p>
                    Anish’s visionary leadership style has also been instrumental in driving the company’s
growth and success. He fosters a culture of innovation, collaboration, and continuous
improvement, empowering his team to be their best and to deliver exceptional results to
clients.
 
                </p>
                <p>
                    As a modern-day CEO, Anish recognises the importance of diversity and inclusion in the
workplace. He is committed to creating a work environment where everyone feels valued,
respected, and supported to achieve their full potential.

                </p>
                <p>
                    Overall, Anish’s extensive healthcare experience, visionary leadership and commitment to
excellence have made him a well-respected and sought-after leader in the healthcare
staffing industry.
                </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>



<?php include 'master/footer.php' ?>
